# Call replay on all the files in a directory to generate images.
#for f in results/Sx_colors_04212017-100.csv
for f in results/Sx_colors_04212017-*.csv
    do python replay.py --flankers 2 --debug --speed -1 --movie --nowait $f;
done
