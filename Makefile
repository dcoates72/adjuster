all:

t10m0: t10m0f t10m0u
t10m0f:
	python color_parse.py '_1_t10_m0'  > t10m0f.txt
	bash crop_color_auto.sh t10m0f.txt
t10m0u:
	python color_parse.py '_0_t10_m0'  > t10m0u.txt
	bash crop_color_auto.sh t10m0u.txt

t10m10: t10m10f t10m10u
t10m10f:
	python color_parse.py '_1_t10_m-10'  > t10m10f.txt
	bash crop_color_auto.sh t10m10f.txt
t10m10u:
	python color_parse.py '_0_t10_m-10'  > t10m10u.txt
	bash crop_color_auto.sh t10m10u.txt

t0m10: t0m10f t0m10u
t0m10f:
	python color_parse.py '_1_t0_m-10'  > t0m10f.txt
	bash crop_color_auto.sh t0m10f.txt
t0m10u:
	python color_parse.py '_0_t0_m-10'  > t0m10u.txt
	bash crop_color_auto.sh t0m10u.txt

ecvp_set:
	# Generate filenames
	rm -f crop_ecvp_set.txt/*
	python color_parse.py '_0_t10_m0'  > ecvp_set.txt
	#egrep '\-9[789]|\-100|-101' ecvp_set.txt > wow
	egrep "\-26|-21|-24|-84|-116" ecvp_set.txt > wow
	mv wow ecvp_set.txt
	cp ecvp_set.txt ecvp_set_mono.txt
	#
	bash crop_color_auto_mono.sh ecvp_set_mono.txt
	bash crop_color_auto.sh ecvp_set.txt

ecvp_set_f:
	rm -f crop_ecvp_set.txt/*
	python color_parse.py '_1_t10_m0'  > ecvp_set.txt
	#egrep '\-9[789]|\-100|-101' ecvp_set.txt > wow
	egrep "\-39|-66|-122|-123|-40" ecvp_set.txt > wow
	mv wow ecvp_set.txt
	bash crop_color_auto_mono.sh ecvp_set.txt

allgoodsubs:
	#bash auto_replay.sh results/S[7-9]*.csv results/S1[01]*.csv
	bash auto_crop_expt.sh results/S[7-9]*.png results/S1[01]*.png
	bash auto_html_expt.sh crop/results/S[7-9]*.png crop/results/S1[01]*.png

    ## Convert result CSV files from dos format CRLFs to "normal" unix
    #for f in `git status | grep mod | grep csv | awk '{print $2;}'`; do dos2unix $f ; done

