import numpy as np
import sequence

# These could change per experiment:
subject_name="S11"
cond="defaults" #1.5deg, 2.5deg spacing

stim1="gmghjhgcni"  # bird
stim2="kckmdfdk"    # table
stim3="adhdlabfcp"  # spaceship (?)
stim4="ameoikkm"    # flag w/line
stim5="fmikfafd"    # stick figure
stim6="klkaclap"    # handlebars
stimAx="akilefmaco"      # upside-down A + cross
stimGrid="adehilmpambncodp"    # grid

target_code=""

target_letter="letters/ln_upperC.csv"

# -10 is to the left
target_pos_x_deg=10.0
match_pos_x_deg=0
clutter_Xs=False

#########################################3

winsize=[1024,768]
ppd=26.3
len_deg=1.5
flanker_spacing=2.5*ppd

limits_x=[-winsize[0]/2.0+10.0,500]
scaler_length=-1.0 # up to lengthen
scaler_pos=0.5
scaler_ori=np.pi/winsize[0]
flanker_spacing=2.5*ppd
length_random_limits=(ppd/2.0,ppd*2.0)
pos_random_limits=[(-ppd/2.0,ppd/2.0),(-ppd/2.0,ppd/2.0)]
# foveal also removes fixation with (height=0)
reset_to_origin=True
cycle_to_menu=False
target_randstim=True
flankers_randstim=False

clutter_lines=False
rand_lines=[4,6]

enable_sounds=False
allow_stim_view=True

arc_num_points=20 # how many points comprise the half-circle arc

button_rotate=0
button_cycle=0
button_menu=0
button_menu_cycle=False # whether the button to cycle curr also goes to add screen
icon_select_maxval=100.0

enable_gc_menu=True
gc_menu_hold_duration=2.0
gc_menu_yloc_min=150
gc_fmatch_hide=3*ppd

blink_rate=np.pi*6.0
blink_duration=0.7
timer_duration=1.0
gc_menu_enter_duration=0.2 # need to be in the top of the screen to trigger menu.. just few frames
mute_duration=0.5
hold_duration=1.2
buzz_duration=5.0 # buzz every 5 seconds if they aren't fixating

contrast_icons_dim=-0.8
contrast_icons_selected=-0.9
contrast_icons_unselected1=0.9
contrast_icons_unselected2=0.9

params_fixation={
    'pos':(0,0),
    'color':(-1,-1,-1),
    'text':'+',
    'height':30,
    'ori':0
    }

params_fixation2={
    'pos':(0,-3*ppd),
    'color':(-1,-1,-1),
    'text':'+',
    'height':30,
    'ori':0
    }

params_win={
    'size':winsize,
    'allowGUI':False,
    'rgb':(0,0,0),
    'units':'pix',
    'fullscr':False,
    'winType':'pyglet',
    'screen':0,#1,
    #'blendMode':'add',
    #'useFBO':True }
    }

params_widget_line={
    'lineWidth':ppd/8.0,
    'lineColor':(1,1,1),
    'units':'pix',
    }

params_line={
    'pos': [ppd*target_pos_x_deg,0],
    'length':ppd*len_deg,
    'ori': np.pi/2.0,
    }

# If this is line is active, parameters from the line # specified override all parameters specified above
sequence.sequence_from_file("seq_test.csv",1,locals())

condition="%s_%d_t%d_m%d"%(cond,int(clutter_Xs),target_pos_x_deg,match_pos_x_deg)
