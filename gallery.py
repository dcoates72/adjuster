import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core
#from psychopy import log
import psychopy.data as pd 
import numpy as np
import time
import matplotlib.pyplot as pyplot

import util
import stims
import importlib

class Experiment:
    def __init__(self, param_filename, docalib=True):
        self.params=importlib.import_module(param_filename)
        self.docalib=docalib

    def makeStims(self, myWin, params):
        #self.targets = stims.OddballLetterRing( name="targets", win=myWin, radius=(params.radius ) )
        #if params.flankers:
            #flankers_i = stims.LetterRing( name="flankers_i", win=myWin, text=params.char_flanker_i, radius=(params.radius-params.flanker_spacing ) )
            #flankers_o = stims.LetterRing( name="flankers_o", win=myWin, text=params.char_flanker_i, radius=(params.radius+params.flanker_spacing ) )
            #stims_all = [self.targets,flankers_i,flankers_o]
        #else:
            #stims_all = [self.targets]

        if True:
            # Sloan rings
            rings = stims.FlankedRings( "rings", myWin, params)
        #sqsize=params.size
        #self.dotring = stims.DotsRing( "dots", myWin, params, sqsize=sqsize)
        #stims_all = [rings, self.dotring]
            stims_all = [rings]

        else:
            rings = stims.Line2Ring( win=myWin, params=params)
            flankers = stims.FlankerRings( "flankers", win=myWin, params=params)
            stims_all = [rings,flankers]

        return stims_all,[rings]

    def run(self):
        params=self.params

        # Set up the screen, output file, etc.
        myWin = visual.Window(params.screendim, allowGUI=True, rgb=(0,0,0), units='pix',
                    fullscr=params.fullscr, winType='pyglet' ) #, blendMode='add', useFBO=True )
        myWin.setMouseVisible(False)
        myWin.setRecordFrameIntervals(True)
        #log.console.setLevel(log.ERROR)

        # Fixation (&helper?):
        # Need to create this first in order to do calibation
        fixation = visual.TextStim(myWin,pos=params.fixation_pos,alignHoriz='center', alignVert='center', height=9, color=(1.0,1.0,1.0), ori=0) 
        fixation.setText( params.fixation_char )

        fixation.setHeight( params.fixation_size )
        fixation.setText( params.fixation_char )
        fixation.draw()

        #myWin.blendMode='add'

    #allseq=np.concatenate( [line2_Ts, line2_Ls, line2_Ss, line2_Vs, line2_V2s] )
        done=False
        whiches=[5,1,5]
        while done==False:
            for ncol,typ in enumerate( np.arange(10)): #whiches) :# [stims.line2_Ts, stims.line2_Ls, stims.line2_Ss, stims.line2_Vs, stims.line2_V2s] ):
                for nrow,_ in enumerate(np.arange(20)) :
                    stimparams={'win':myWin, 'name':None, 'size':params.size/2.0, 'lineWidth':params.size/5.0 }
                    edges=(typ%4+2)# np.random.randint(1,7)
                    #if ntyp==1:
                        #edges=np.random.randint(2,5)
                    stim=stims.TwoLinesConnected( win=myWin, name='', pos=(-460+100*ncol,300-150*nrow), size=30, lineWidth=5, edges=edges )
                    #stim=stims.LinesConnected(win=myWin, name='', pos=(-400+200*ntyp,300-200*nwhich), size=30, lineWidth=5, edges=ntyp+2 )
                    #stim.update(a2l)
                    stim.generate()
                    stim.update()
                    #myWin.blendMode='add'
                    stim.draw()

                    #stim=stims.TwoLinesConnected2(win=myWin, name='', pos=(100+80*ncol,300-150*nrow), size=30, lineWidth=5, edges=edges )
                    #stim=stims.LinesConnected(win=myWin, name='', pos=(-400+200*ntyp,300-200*nwhich), size=30, lineWidth=5, edges=ntyp+2 )
                    #stim.update(a2l)
                    #stim.generate()
                    #stim.update()
                    #myWin.blendMode='add'
                    #stim.draw()

            myWin.flip()
            #myWin.blendMode='add'

            myWin.getMovieFrame()   # Defaults to front buffer, I.e. what's on screen now.
            myWin.saveMovieFrames('screenshot.png')
            key=event.waitKeys()
            
            done='escape' in key


if __name__ == "__main__":
    Experiment("params_gallery",docalib=False).run()
