# -*- coding: utf-8 -*-
# Gaze contingent presentation based on psychopy demo script
# Nat Melnik
# C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\hw\sr_research\eyelink\eyetracker.py
# supported dev: C:\PsychoPy2\Lib\site-packages\PsychoPy-1.84.2-py2.7.egg\psychopy\iohub\devices\eyetracker\supported_config_settings.yaml
from psychopy import visual, core, monitors,event
from psychopy.data import TrialHandler,importConditions
from psychopy.iohub import (ioHubExperimentRuntime, module_directory,
                            getCurrentDateTimeString)
import os
import numpy

from adjust_expt import adjust_experiment
import adjust_params as params # also do it the more principled day

thresh_pixels=50

class ExperimentRuntime(ioHubExperimentRuntime):
    def run(self,*args):

        display=self.hub.devices.display
        kb=self.hub.devices.keyboard
        mouse=self.hub.devices.mouse
        tracker=self.hub.devices.tracker
        tracker.runSetupProcedure()
        res=display.getPixelResolution() # Current pixel resolution of the Display to be used
        coord_type=display.getCoordinateType()

        monitorsetting = monitors.Monitor('m1', width=39.5, distance=57)
        monsize =  [1024, 768]
        monitorsetting.setSizePix(monsize)
        window=visual.Window(res,monitor=monitorsetting, units='pix', fullscr=True,allowGUI=True,color=[0,0,0], screen=display.getIndex())
        #self.window=window


        mouse.setSystemCursorVisibility(False)
        clock = core.Clock()

        fixcircle = visual.Circle(win=window, radius=6, pos=(0, 0), lineColor=[1,0,0],
                                  edges=120,fillColor=[-1,-1,-1], units='pix')

        gaze = visual.Circle(win=window, radius=6, pos=(0, 0), lineColor=[0.9,0.9,0.9],
                                  edges=120, fillColor=[0.9,0.9,0.9], units='pix')

        instructions_text_stim = visual.TextStim(window, text='', pos = [0,0],
                                    height=20, color=[-1,-1,-1], colorSpace='rgb',
                                    alignHoriz='center', alignVert='center',
                                    wrapWidth=window.size[0]*.9)

        #bulb=visual.Circle(win=window_other, radius=2000, pos=(0, 0), units='pix', fillColor=[-1,-1,-1] )
        #fixcircle_other = visual.Circle(win=window, radius=0.1, pos=(0, 0), lineColor=[-1,-1,-1],

        instuction_text="Press Space to Start"
        instructions_text_stim.setText(instuction_text)
        instructions_text_stim.draw()
        flip_time=window.flip()
        self.hub.clearEvents('all')
        #kb.waitForPresses(keys=[' '])
        #fixcircle.draw()
        #kb.getKeys(keys=[' '])
        #for trial in trials:
        
        manual_mode=False #If False, manual mode
        manual_lit=False

        run_trial=True
        if True:
                self.hub.clearEvents('all')
                tracker.setRecordingState(False)

        expt=adjust_experiment()
        expt.setup(win=window)
                
        show_gaze=False
        show_gaze_menu=True # temp for gaze showing in the menu
        done=False
        extra_key=""
        while (run_trial is True) and (done==False):
            clock.reset()
            self.hub.clearEvents('all')
            tracker.setRecordingState(True)

            # See if there are any keys to process
            #for event in kb.getEvents():
            # event.key.. This way also gets release messages
            # would need to check that event.type==Keyboard.KEY_PRESS
            for key in kb.getPresses():
                if key in ['q','escape']:

                    run_trial=False
                elif key in [' ']:
                    manual_mode=True
                    manual_lit=not( manual_lit ) #invert
                elif key in ['0']:
                    manual_lit=False
                elif key in ['1']:
                    manual_lit=True
                elif key in ['t']:
                    manual_mode=False
                elif key in ['g']:
                    show_gaze=not (show_gaze)
    
            gpos=tracker.getPosition()
            gaze_ok=False # only draw if gaze is detected and near the fovea
            show_gaze_menu=False # default is not in menu
            if type(gpos) in [tuple,list]:
                gaze.pos = gpos
                if ((abs(gpos[0]) < thresh_pixels ) and (abs(gpos[1]) < thresh_pixels)):
                    fixcircle.lineColor=(-1,-1,-1)
                    gaze_ok=True
                    #show_gaze=False
                else:
                    if gpos[1]>params.gc_menu_yloc_min:
                        show_gaze_menu=True
                        fixcircle.lineColor=(1,0,0)

            if show_gaze or show_gaze_menu:
                gaze.draw()
                #fixcircle.draw()

            done,extra_key=expt.do_frame(drawall=gaze_ok, doflip=False, gpos=gaze.pos) 
            flip_time=window.flip()

            if extra_key=='g':
                show_gaze=not (show_gaze)

        tracker.setRecordingState(False)
        tracker.setConnectionState(False)
        window.close()
        self.hub.quit()
        core.quit()
        expt.finish()
        
runtime=ExperimentRuntime(module_directory(ExperimentRuntime.run), "experiment_config.yaml")
runtime.start()

