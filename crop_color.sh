# Run this with a number.
#
# It will call auto_stim.py to generate the CSV from the favorite
#   'simulating' an experiment using this stimulus.
# Then it calls replay.py to generate the png. 'screenshot' image from the CSV. 
# Then it crops the image.

#python auto_stim.py $1 > stim$1.csv
#python replay.py  --speed -1 --movie --nowait stim$1.csv
convert $1 -crop 1024x100+0+340 crop/$1
