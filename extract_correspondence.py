import adjust_params as params
import stims
import numpy as np
import matplotlib.pyplot as plt
from scipy.optimize import curve_fit

def gaus(x,a,x0,sigma):
    return a*np.exp(-(x-x0)**2/(2*sigma**2))

def fitgauss(x,y, interp=False):

    x=x.copy()
    y=y.copy()
    if interp and (len(x)==6):
        # For the -2/+2 ciunt data, interpoltae a point
        x[5]=(x[1]+x[2])/2.0
        y[5]=(y[1]+y[2])/2.0
        x[4]=(x[2]+x[3])/2.0
        y[4]=(y[2]+y[3])/2.0
        #print x,y

    n = len(x)                          #the number of data
    men = sum(x*y)/n                   #note this correction
    sigma = sum(y*(x-men)**2)/n        #note this correction
    popt,pcov = curve_fit(gaus,x,y,p0=[1,men,sigma])
    return popt,pcov

def fitk(data):
    import scipy.stats
    kernel=scipy.stats.gaussian_kde(data)
    pos=np.linspace(data.min(), data.max() )
    Z = np.reshape(kernel(pos).T, pos.shape)
    return pos,Z

def plotcorr(which,a,b,valids,doplot=True):
    #print a,valids
    valids=valids[0:len(b)]
    avals=a[valids,which]
    bvals=b[valids,which]

    # Spatially Normalize
    if (which==0) or (which==1):
        avals-=np.mean(avals)
        bvals-=np.mean(bvals)

    # Convert to degrees
    if (which==0) or (which==1) or (which==3):
        avals/=params.ppd
        bvals/=params.ppd
    
    minval=np.min( ([avals,bvals]) )
    maxval=np.max( ([avals,bvals]) )
    if doplot:
        plt.scatter( avals, bvals, alpha=0.8)
        plt.plot( [minval, maxval], [minval, maxval], 'k:' )
        plt.show()

    diffs=avals-bvals
    if which==2:
        mask=( diffs>np.pi/2.0 )
        diffs[mask] -= np.pi

        diffs = diffs/np.pi*180.0

    return avals,bvals,diffs

def pair2seg(points):
    pt0=points[0]
    pt1=points[1]
    diff=pt0-pt1
    center=np.array(pt0+pt1)/2.0
    length=np.linalg.norm( diff )
    angle=np.arctan2( diff[1], diff[0] ) % np.pi
    return np.array( [center[0],center[1],angle,length] )

def pts2segs(points):
    segs=np.array([pair2seg(apair) for apair in points])
    return segs

def process_target(code):
    # Copied from adjust_expt:
    size=1.0*params.ppd
    lw=size/8.0

    verts=stims.code2points(code)
    pos=(0,0) #todo
    points=stims.makerel2( pos, size, verts)
    segs=pts2segs( points )
    return segs
    

def extract(filename):
    fil=open(filename,"rt")
    lins=fil.readlines()

    segs={} # Use a dictionary to know when segs are the same by keying with the 4 parameters (param_spec)
    maxid=0
    for alin in lins:
        fields=alin.split(',')
        if fields[1][0:2]=='lc':
            code=fields[1][3:]
            if code=='aa':
                continue
            target=process_target(code)
        if len(fields)<10:
            continue
        if (fields[1] != 'ori') or (fields[2] != "0.000000"):
            continue
        param_spec="%s,%s,%s,%s"%(fields[6], fields[7], fields[8], fields[9].strip())
        #uniq="%d-%d-%d-%g-%d"%(fields[5], fields[6], fields[7], fields[8], fields[9])
        segnum=int(fields[5])
        segs[param_spec]=segnum

    maxid=np.max( segs.values() )
    arr=np.zeros( (maxid+1,4))
    for akey in segs.keys():
        entry=arr[segs[akey]]
        fields=akey.split(',')
        entry[0]=int(fields[0])
        entry[1]=int(fields[1])
        entry[2]=float(fields[2]) % np.pi
        entry[3]=int(fields[3])
        
    valids=np.any( (arr!=0), 1)
    return segs,target,arr,valids

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help='Filename')
    parser.add_argument('--debug', help='Show log messages', action='store_const', dest='debug', const=True, default=False  )
    args = parser.parse_args() 

    if args.debug>0:
        logging.basicConfig(level=logging.INFO)

    segs,target,arr,valids=extract(args.filename)
    ret=plotcorr( 2, arr, target, valids)


