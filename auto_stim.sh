# Run this with a number.
#
# It will call auto_stim.py to generate the CSV from the favorite
#   'simulating' an experiment using this stimulus.
# Then it calls replay.py to generate the png. 'screenshot' image from the CSV. 
# Then it crops the image.

python auto_stim.py $1 > stim$1.csv
python replay.py  --speed -1 --movie --nowait stim$1.csv
convert stim$1.csv.png -crop 100x100+720+340 stim_crop/stim$1.png 
