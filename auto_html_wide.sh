# Generate an HTML 'gallery', which is really just a big list of the images, with clickable thumbnails.

ls -1tr *.png |  awk 'BEGIN {print "<!DOCTYPE html><html><body>"}; {print "<br>",$0,"<br><a href=\"",$0,"\"><img src=\"",$0,"\" width=512 height=50></a>"}; END {print "</body></html>"};' > gallery.html

