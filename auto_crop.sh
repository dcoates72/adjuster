# Crop the letter images to approx. correct size.

for f in letters/*.png 
    do convert $f -crop 80x80+480+350 crop/$f
done
