#!/bin/bash

#Automatically create a "gallery" subdirectory of images and html file from a directory of csv files.

#REPLAY="python /home/dcoates/dev/adjuster/replay.py --speed -1 --threshold 200 --movie --nowait --flankers 2"
REPLAY="python /home/dcoates/dev/adjuster/replay.py --speed -1 --movie --nowait --flankers 2 --recolor --colorize" # --flankers 2 --recolor --colorize

INPUT_DIR=../adjust_match_pilot/subs2_color
INPUT_FILES=Sx_colors_04212017-3*.csv 
GALLERY_DIR=gallery_test

mkdir -p $GALLERY_DIR

for f in $INPUT_DIR/$INPUT_FILES;
    do $REPLAY $f;
    base=${f##*/}
    echo $base
    
    convert $INPUT_DIR/$base.png -crop 1024x100+0+340 $GALLERY_DIR/$base.png
    #convert $INPUT_DIR/$base.png -crop 100x100+450+340 $GALLERY_DIR/$base.png
    rm -v $INPUT_DIR/$base.png
    #mv -v $f.png $GALLERY_DIR;
done 

# Create an html file with thumbnails of all the entries
cd $GALLERY_DIR
ls -1tr *.png |  awk 'BEGIN {print "<!DOCTYPE html><html><body>"}; {print "<a href=\"",$0,"\"><img src=\"",$0,"\" width=192 height=256></a>"}; END {print "</body></html>"};' > gallery.html

#ls -1 results/* |  awk 'BEGIN {print "<!DOCTYPE html><html><body>"}; END {print "</body></html>"}; {print "<a href=\"",$0,"\" style=\"display: inline-block; width: 256px; height: 192px; background-image: url(\'",$0,"\')> </a>"};' 

