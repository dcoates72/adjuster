import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core
import psychopy.data as pd 
import numpy as np

from adjust_stims import stim_factory
import adjust_params as params
from auto_corr import getcond

import extract_correspondence as ec

def auto_afc(filename="allres.txt", cond_show='F', flanked_show=False):
    fil=open(filename,"rt")
    lins=fil.readlines()

    print ("%s %d"%(cond_show,flanked_show) )

    vals=[]
    numsegs=[]
    numtargs=[]
    count=0
    win = visual.Window(**params.params_win)
    for nlin,alin in enumerate(lins):
        fields=alin.split(' ')
        cond,crowded=getcond(fields[0])
        if fields[0][0]=='#':
            continue
        if (cond!=cond_show) or (crowded!=flanked_show):
            continue

        if flanked_show==1:
            cond2='%sX'%cond_show
        else:
            cond2='%s'%cond_show

        rater0="results/Sx_colors_04212017-%s.csv"%fields[1]
        segs,target,arr,valids=ec.extract(rater0)

        rand_which=np.random.randint(4)
        print ('%s %d'%(fields[1], rand_which) )

        show1( win, segs, target, arr, which_quad=rand_which)
        win.getMovieFrame(buffer='back') # dopn't have to show
        movie_filename='nafc/%s/nafc-%s.png'%(cond2,fields[1])
        win.saveMovieFrames(fileName=movie_filename)

        win.clearBuffer()

    win.close()

def jits(whichcond,flanked,whichp):
    flanked_char=""
    if flanked:
        flanked_char="_X"
    fname='%s%s-%d.npy'%(whichcond,flanked_char,whichp)
    f=np.load(fname)
    pickone=np.random.randint(len(f))

    val=float( f[pickone] )
    if whichp==2:
        val=val/180.*np.pi # deg to radians
    else:
        val *= params.ppd  # deg to pixels
        
    return pickone,val,f

def show1(win, segs_m, target, arr, which_quad=0):
    segs_target=[stim_factory( 'Line', win, nseg, params.params_line, params.params_widget_line,
                    None, None, False) # last three: outfile, time_start, colorize
                for nseg,aseg in enumerate(target) ]

    segs_quad=[[stim_factory( 'Line', win, nseg, params.params_line, params.params_widget_line,
                    None, None, False) # last three: outfile, time_start, colorize
                for nseg,aseg in enumerate(target) ]
                for n in np.arange(4) ]

    segs_match=[stim_factory( 'Line', win, nseg, params.params_line, params.params_widget_line,
                    None, None, False) # last three: outfile, time_start, colorize
                for nseg,aseg in enumerate(arr) ]

    for nseg,aseg in enumerate(segs_target):
        aseg.params['pos']=[target[nseg][0], target[nseg][1]]
        aseg.params['ori']=target[nseg][2]
        aseg.params['length']=target[nseg][3]
        aseg.draw()

    quadp=[ [-200,-200], [-200,200], [200,200], [200,-200]]
    for nqua,qua in enumerate(segs_quad):
        if nqua==which_quad:
            for nseg,aseg in enumerate(segs_match):
                xm=np.mean(arr[:,0])
                ym=np.mean(arr[:,1])
                aseg.params['pos']=[arr[nseg][0]+quadp[nqua][0]-xm, arr[nseg][1]+quadp[nqua][1]-ym]
                aseg.params['ori']=arr[nseg][2]
                aseg.params['length']=arr[nseg][3]
                aseg.draw()
        else:
            for nseg,aseg in enumerate(qua):
                aseg.params['pos']=[target[nseg][0]+quadp[nqua][0], target[nseg][1]+quadp[nqua][1]]
                aseg.params['ori']=target[nseg][2]
                aseg.params['length']=target[nseg][3]
                aseg.nudge('pos', [jits("F",0,0)[1], jits("F",0,1)[1]] )
                aseg.nudge('ori', jits("F",0,2)[1] )
                aseg.nudge('length', jits("F",0,3)[1] )
                aseg.draw()

if __name__=="__main__":
    rater0="results/Sx_colors_04212017-%s.csv"%13
    segs_m,target,arr,valids=ec.extract(rater0)
    win = visual.Window(**params.params_win)
    show1( win, segs_m, target, arr)
    win.flip()
    event.waitKeys()
    win.close()
