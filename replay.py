import numpy as np
import matplotlib.pyplot as pyplot
import time
import util
import importlib
import datetime
import copy
import os,sys

import stims

import logging

import adjust_params as params
from adjust_stims import stim_factory

f_time=0
f_op=1
f_val0=2
f_val1=3
f_class=4
f_id=5
f_pos0=6
f_pos1=7
f_ori=8
f_length=9

delta_threshold=200 # maximum pixel movmt allowed before it's considered a false jump

file_version=0.0 # TODO: put in an instance, not a global

glob_code=""
glob_current=0
glob_header_mode=False

def create_unique_id(dict_stim):

    # Start at #30 for renumberfix features
    #current=int( min( dict_stim.keys() ) )
    current=30
    while '%s'%current in dict_stim:
        current+=1
    return str(current)

def parse1line(lin, win, num_matches, dict_stim, rand_targets, flankers='', target_loc=None, recover=False, recolor=False, colorize=False):
    global file_version, glob_code, glob_current, glob_header_mode

    fields=lin.split(",")
    if len(fields)==10:
        # Normal line type

        # Build list of lines to ignore
        ignores=[]
        if not (flankers=='12' or flankers=='1'):
            ignores += ['100', '101' ]
        if not (flankers=='12' or flankers=='2'):
            ignores += [str(s) for s in np.arange(200,208)]

        if fields[f_id] in ignores: #'10','11','100','101']: # REF line for LN letters, valid for others!!
             logging.info( "ignore %s"%fields[f_id] )
             return []

        if recolor and (fields[f_op]=='ori') and (float(fields[f_val0])==0):
            if glob_header_mode:
                #print "skips"
                # If ori==0 sentinel value, but already exists,
                # skip it.. (i.e., msg after color change, but odn't need)
                return fields

        exists=fields[f_id] in dict_stim
        if exists==False:
            stim=stim_factory( fields[f_class],
                win, int(fields[f_id]), params.params_line, params.params_widget_line,
                None, None, colorize) # last three: outfile, time_start, colorize
            dict_stim[ fields[f_id] ] = stim
            logging.info( "new: %s"%(fields[f_id]) )
        else:
           stim=dict_stim[ fields[ f_id ] ]

        if fields[ f_op]=='del':
            logging.info( "del: %s"%fields[f_id] )
            stim.deleted=True

        # See how much changed...
        newpos=np.array( (fields[f_pos0],fields[f_pos1]), dtype='float' )
        newori=float( fields[f_ori])
        newlength=float( fields[f_length])

        delta_pos=newpos-stim.params['pos']
        delta_ori=newori-stim.params['ori']
        delta_length=newlength-stim.params['length']

        if ( recover and (((any(delta_pos)) and delta_ori) or (any(np.abs(delta_pos)>delta_threshold)))):  
            try:
                if not (stim.isnew):
                    logging.info("Pos+ori changed and not new !!")
                    logging.info("New:%s,%s Old:%s,%s Delta:%s,%s"%(str(newpos),newori,str(stim.params['pos']),stim.params['ori'],
                        str(delta_pos),str(delta_ori) ) )

                    oldcode=stim.idcode # remember the old number

                    reused=-1
                    # TODO: First see if this might be a re-movement of an existing (other) feature
                    for stim_temp in dict_stim.values():
                        temp_delta_pos=newpos-stim_temp.params['pos']
                        temp_delta_ori=newori-stim_temp.params['ori']
                        if ( (temp_delta_ori==0) and all(np.abs(temp_delta_pos)<delta_threshold)) or (not(temp_delta_ori==0) and not any(temp_delta_pos) ):
                            logging.info("Detected reuse of %s (pos_delta=%s ori_delta=%s)"%(stim_temp.idcode, str(temp_delta_pos), str(temp_delta_ori)) )
                            reused=stim_temp.idcode
                            stim=stim_temp

                    if reused==-1:
                        # Nothing similar found, just create a new feature
                        newid=create_unique_id(dict_stim)
                        logging.info("Renumbering %s to %s, using %s for new element."%(oldcode,newid,oldcode))
                        stim.idcode=newid
                        dict_stim[ newid ] = stim

                        # Fix: Create a new stimulus using the bad duplicated number
                        stim=stim_factory( fields[f_class], win, oldcode, params.params_line, params.params_widget_line, None, None) # last two: outfile, time_start
                        stim.isnew=False
                        dict_stim[ oldcode ] = stim

                # Else it's okay: this jumped, but it didn't exist before. Continue..
            except AttributeError:
                # This will only happen immediately after creation: (the first time isnew is purposely undefined)
                stim.isnew=False

        # Then move to new position
        stim.params['pos']=newpos
        stim.params['ori']=newori
        stim.params['length']=newlength
    else:
        if fields[ f_op]=='ver':
            logging.info( "ver: %s"%fields[f_val0] )
            file_version=fields[f_val0]
        elif recolor and (fields[f_op]=='sel'):
            glob_current=fields[ f_val0] 
            logging.info( "sel: %s"%fields[f_val0] )
            glob_header_mode=True
        elif recolor and (fields[f_op]=='color'):
            logging.info( "color: %s glob_current=%s"%(fields[f_val0],str(glob_current) ) )
            glob_header_mode=True
            newid=int( float( fields[f_val0] ) )
            # It's possible the numbering is off, so 
            stim_cur=None
            while stim_cur==None:
                try:
                    stim_cur=dict_stim[str(int(float(glob_current)))]
                except KeyError:
                    print "%s failed"%(str(glob_current))
                    glob_current = float(glob_current) + 1
            stim_cur.idcode=newid
        elif (fields[f_op][0:3]=="lc:") or (file_version<1.0): # code for randstim
            if file_version < 1.0: # old-style. The only short lines in the file were 2lc add codes
                code=fields[f_time]
            else:
                # version 1.0 or later, saved as "lc:xxxx" in the op 
                code=fields[f_op][3:]

            glob_code=code # todo: 

            # Assume it's the 3-line randstim specification for 2Line stimuli
            size=1.0*params.ppd #TODO
            lw=size/8.0
            edges=len(code)/2

            if target_loc==None:
                target_loc=params.target_pos_x_deg*params.ppd
            else:
                target_loc=target_loc*params.ppd

            logging.info( 'randstim=%s @ %d'%(code,target_loc) )
            randstim=stims.TwoLinesConnected(win=win, name='', pos=(target_loc,0), size=size, lineWidth=lw, edges=edges, colorize=colorize)#1.0*ppd/5.0, edges=edges )
            randstim.update(code)
            rand_targets += [ randstim ]

    return fields

def replay(filename, speed=1, flankers='', save_movie=False, nowait=False, auto_recover=False, step_mode=False,
        debug=False, threshold=delta_threshold, recolor=False, colorize=False):
    # Show the ``movie'' using psychopy
    import psychopy.visual as visual
    import psychopy.event as event
    import psychopy.core as core
    import psychopy.data as pd 

    # Inits:
    dict_stim={}
    rand_targets=[]
    win = visual.Window(**params.params_win)
    frame_number = visual.TextStim(win, alignHoriz='left', alignVert='top',pos=(-win.size[0]/2.0,win.size[1]/2.0), text='FRAME', color=(-1,-1,-1) )
    num_matches=0

    speed=int(speed)

    fil=open(filename,"rt")
    lins=fil.readlines()
    abort=False
    target_loc=10
    if filename.find("_t0")>=0:
        target_loc=0

    time_label_last="TIME"

    for nlin,alin in enumerate(lins):
        fields=parse1line( alin, win, num_matches, dict_stim, rand_targets, flankers=flankers, target_loc=target_loc,
            recover=auto_recover, recolor=recolor, colorize=colorize) # propagate cmd-line args

        if len(fields)==0:
            continue # ignores come back as empty list

        if fields[f_op]=='gaze':
            continue

        if len(fields)==10:
            time_label_last='f=%s t=%s'%(str(nlin), fields[f_time] )

        if (speed>0) and (nlin%speed==0):
            for astim in dict_stim.values():
                astim.draw()

            if len(fields)==10:
                frame_number.text='f=%s t=%s'%(str(nlin), fields[f_time] )

            frame_number.draw()
            [target.draw() for target in rand_targets]
            win.flip()
            if save_movie:
                win.getMovieFrame()

        if step_mode:
            anykeys=event.waitKeys()
        else:
            anykeys=event.getKeys()
        if len(anykeys)>0:
            if anykeys[0] in ['escape']:
                abort=True
                break
            if anykeys[0] in ['j']:
                speed=-1
            if anykeys[0] in ['s']:
                step_mode=not (step_mode)

    # TODO: Can't get last frame/time if it's a gaze--too slow etc. Fix is to save the time/text 
    # i.e., "lastvalid" and set it when we get below.

    # Show final frame
    # DC: Maybe this was necessary before since it was plotting from the back buffer, instead of the front?
    if abort==False:
        #if len(fields)==10:
            #frame_number.text='f=%s t=%s'%(str(nlin), fields[f_time] )
        frame_number.text=time_label_last #'f=%s t=%s'%(str(nlin), fields[f_time] )
        # show for one last time
        for nstim,astim in enumerate(dict_stim.values()):
            astim.draw()
        frame_number.draw() # should still have number from before
        [target.draw() for target in rand_targets]
        win.flip()
        if save_movie:
            win.getMovieFrame()
        if nowait==False: # normally wait for a key, unless over-ride
            event.waitKeys()

    fil.close()
    #dict_stim={}
    if save_movie:
        if speed==-1: # if skipped to last frame, only save that as an image
            movie_filename='%s.png'%filename
        else:
            movie_filename='%s.mp4'%filename
        win.saveMovieFrames(fileName=movie_filename)
    win.close()
    return dict_stim

def rebuild(filename, win=None, colorize=False):
    dict_stim={}
    rand_targets=[]
    num_matches=0
    fil=open(filename,"rt")
    lins=fil.readlines()
    for nlin,alin in enumerate(lins):
        parse1line( alin, win, num_matches, dict_stim, rand_targets, colorize=colorize )
    fil.close()
    return dict_stim,glob_code

if __name__=="__main__":
    import argparse
    parser = argparse.ArgumentParser()
    parser.add_argument('filename', help='Filename')
    parser.add_argument('--speed', help='Playback speed (2=twice, etc.) -1 jumps to end', default=1, nargs='?') # nargs=? : optional
    parser.add_argument('--threshold', help='Smallest delta to consider a false jump', default=40, nargs='?'  )
    parser.add_argument('--flankers', help='Show flanker types=(1,2, or 12)', default='2', nargs='?'   )
    parser.add_argument('--debug', help='Show log messages', action='store_const', dest='debug', const=True, default=False  )
    parser.add_argument('--movie', help='Save movie to <filename>.mp4', action='store_const', dest='save_movie', const=True, default=False   )
    parser.add_argument('--nowait', help="Don't pause at last frame (i.e., for batch usage)", action='store_const', dest='nowait', const=True, default=False )
    parser.add_argument('--recover', help="Auto-recover early versions with problems.", action='store_const', dest='auto_recover', const=True, default=False )
    parser.add_argument('--step', help="Step mode", action='store_const', dest='step_mode', const=True, default=False )
    parser.add_argument('--recolor', help='Playback scored color files', action='store_const', dest='recolor', const=True, default=False  )
    parser.add_argument('--colorize', help='Whether to color all line stimuli', action='store_const', dest='colorize', const=True, default=False  )

    args = parser.parse_args() #['filenam', '--speed' ] )

    if args.debug>0:
        logging.basicConfig(level=logging.INFO)

    delta_threshold=int(args.threshold)
    replay( **vars(args) )

    #elif len(sys.argv)==3:
        #replay(sys.argv[1], int(sys.argv[2]))
