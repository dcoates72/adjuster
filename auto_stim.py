# Run this with a number as a parameter and it will create a .csv file that 'simulates' an
# experiment using the appropriate 'stimX' favorite.

import adjust_params
import sys

which=int(sys.argv[1])
code=eval("adjust_params.stim%d"%which)

print "0.0,ver,1.000000,0.000000"
print "0.19000,lc:%s,0.000000,4.000000"%code

