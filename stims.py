import psychopy.visual as visual
import numpy as np
#import params2 as params

colors_segment=[(1,0,0), (0,1,0), (0,0,1), (1,1,0), (0.75,0.75,0.5), (1,0.5,0), (0.5,0.5,0.75), 
    (-1,-1,-1), (1,1,1)]

################################################
# Helpers
################################################
# Rings:
def ring_position(center, radius, N, which_angle, jitter_amt=0.0, phase=0, extent=1.0):
    if extent<1.0:
        # If only filling an arc, go to extent of it
        # Divide by extent so that phase is in actual units, not relative to extent
        if N==1:
            angle=phase/extent
        else:
            angle=2*np.pi/(N-1)*which_angle+phase/extent
    else:
        angle=2*np.pi/N*which_angle+phase/extent
    x=center[0]+np.cos(angle*extent)*radius+jitter_amt*(np.random.rand()*2.0-1.0)
    y=center[1]+np.sin(angle*extent)*radius+jitter_amt*(np.random.rand()*2.0-1.0)
    return (x,y)

# Polar Positions starting from (0th=0deg, at 3 o'clock. Rightmost. counter-clockwise)
def make_ring(center, radius, N, phase=0, jitter_amt=0, extent=1.0):
    return [ring_position( center, radius, N, which_angle, phase=phase, jitter_amt=jitter_amt, extent=extent) for which_angle in range(N)]

################################################
# Base class and single-object classes
################################################
class Stim(object):
    # Base class stim object. Doesn't do much, except calling underlying
    # "_stim" object (psychopy object)'s draw method, which is often sufficient.
    def __init__(self, win=None, params=None):
        self.params=params
        self.win=win
        self._stim=None
    # In drawing loop. Should be fast.
    def draw(self):
        if not(self._stim==None):
            self._stim.draw()
    def setText(self):
        return
    def __repr__(self):
        return "Stim({0.params!r})".format(self)
    # For trials, update things
    def update(self):
        return

def addbars( obj, win, pos, size, color, flanker_type, bar_spacing):
    bar=size/5.0
    wid=bar

    siz=size # to center bar#/2.0+size/2.0
    corn1=size/2.0 # corners aligned with bounding box of char
    corn2=size/2.0+bar/2.0+bar*bar_spacing # corners for box containing flankers
    if flanker_type=="box":
        obj.flankers = [visual.ShapeStim( win, pos=pos, units="pix", vertices=((-corn2,-corn2),(corn2,-corn2),(corn2,corn2),(-corn2,corn2) ),
            closeShape=True, lineColor=color, lineWidth=wid, interpolate=False)] # lines
    elif (flanker_type=="bars") or (flanker_type=="bars2"): # bars:
        lins = [ [(-corn1,-corn2),(corn1,-corn2)], [(corn2,-corn1),(corn2,corn1) ],[(corn1,corn2),(-corn1,corn2)], [(-corn2,corn1),(-corn2,-corn1) ] ]
        obj.flankers = [visual.ShapeStim( win, pos=pos, units="pix", vertices=v, closeShape=False, lineColor=color, lineWidth=wid, interpolate=False)
                for v in lins ]
    
        if flanker_type=="bars2":
            corn2=corn2+bar*2.0 # a bar-width blank line, then line
            lins = [ [(-corn1,-corn2),(corn1,-corn2)], [(corn2,-corn1),(corn2,corn1) ],[(corn1,corn2),(-corn1,corn2)], [(-corn2,corn1),(-corn2,-corn1) ] ]
            obj.flankers += [visual.ShapeStim( win, pos=pos, units="pix", vertices=v, closeShape=False, lineColor=color, lineWidth=wid, interpolate=False)
                for v in lins ]
    elif (flanker_type=="theta4"):
        obj.flankers=[]
        #flankers_theta=[ ['I','f'], ['H','e'] ]  # vertical, horizontal
        #for angl in np.linspace(0,np.pi,2*np.pi,4,endpoint=False):
        #elf.flankers_o = LetterRing( name="flankers_o", win=win, params=params, spokes=self.params.flanker_spokes,
        #       text=self.params.char_flanker_o, radius=(self.params.radius+self.params.flanker_spacing ) )
        #   self._stims = [self.targets,self.flankers_i,self.flankers_o]
        #lse:
        #self._stims = [self.targets]
    else:
        obj.flankers=[]        




class Letter(Stim):
    def __init__(self, win, name, font, pos, size, text, ori, color, params):
        # Prepend with "_" internal (non-serialized) things
        if not(win==None):
            self._stim=visual.TextStim( win, pos=pos, alignHoriz='center', alignVert='center', units='pix',
                height=size, color=color, ori=ori, text=text, font=font)
            self.win=win
        else:
            self.win=None
            self._stim=None

        self.name=name
        self.font=font
        self.pos=pos
        self.size=size
        self.text=text
        self.ori=ori
        self.color=color
        self.params=params
        addbars(self, win, pos, size, color, params.bar_style, params.bar_sep)
        return

    def __repr__(self):
        return 'Letter({0.name!r}, {0.font!r}, {0.pos!r}, {0.size!r}, {0.text!r}, {0.ori!r}, {0.color!r})'.format(self)
    def draw(self):
        self._stim.draw()
        [flanker.draw() for flanker in self.flankers]
        return
    # For each trial, set up stuff
    def update(self):
        return

class Ring(object):
    def __init__(self, pos, ecc, N ):
        self.pos=pos
        self.ecc=ecc
        self.N=N

class StimSet(object):
    def __init__(self,stims=[]):
        self._stims=stims
    def draw(self):
        [astim.draw() for astim in self._stims]
    def update(self, val, trialNum):
        [astim.update(val, trialNum) for astim in self._stims]

class LetterRing(StimSet):
    def regen(self, win, params, center, radius, spokes ):
        self._stims=[Letter(win,name="",font=self.params.font, pos=apos, size=self.params.size, text=self.text, ori=self.params.ori, 
            color=self.color, params=params) for (npos,apos) in enumerate(make_ring(center, radius,
                spokes, phase=self.params.phase, jitter_amt=self.jitter_amt, extent=self.params.ring_extent) ) ] 
        
    def __init__(self,name,win=None, params=None, radius=None, center=(0,0), text="X", spokes=None, jitter_amt=None, color=None):
        self.params=params
        if radius==None:
            radius=self.params.radius
        if spokes==None:
            spokes=self.params.num_spokes
        if jitter_amt==None:
            jitter_amt=self.params.jitter_amt
        if color==None:
            color=self.params.color_target
        self.color=color
        self.jitter_amt=jitter_amt
        self.radius=radius
        self.spokes=spokes
        self.center=center
        self.text=text
        self.win=win
        self.regen( self.win, self.params, self.center, self.radius, self.spokes )

    def update(self,val,trialNum):
        if self.params.jitter_amt > 0: # If jittering, need to reconstruct stimuli each time
            self.regen( self.win, self.params, self.center, self.radius, self.spokes )
        for astim in self._stims:
            astim._stim.height=val
    
        # TODO: adjust nominal spacing of flankers.. should not be here I think though.
        #if self.params.variable=="size":
            #[aflanker.setHeight(val) for astim in self._stims]
            #[aflanker.setHeight(val) for astim in self._stims]

Ts_chars=['T', '<','>','L'] # T pointed: up, L, R, Down
Ts_ori_map={'T':0, '<':90,'>':-90,'L':180} # T pointed: up, L, R, Down

def oddball_select(num_spokes):
    quot=num_spokes/8.0 
    if num_spokes>=8:
        target_element = np.random.randint(0,8)*quot
        target_response=target_element/quot
    else:
        target_element = np.random.randint(0,num_spokes)
        target_response=target_element/quot
    return target_element,target_response

class OddballLetterRing(LetterRing):
    def __init__(self,name,win=None,params=None, radius=None,center=(0,0) ):
        self.params=params
        if radius==None:
            radius=self.params.radius
        LetterRing.__init__(self,name,win,params=params,radius=radius,center=center)
        if self.params.foveal_helper:
            self._helper=Letter(win,name="Helper",font=self.params.helper_font, pos=center,
                size=self.params.size, text="X", ori=self.params.ori, color=self.params.color_target, params=params)
    def update(self,val,trialNum):
        LetterRing.update(self, val, trialNum) # call base class, esp. for flanker jitter update
        
        # oddball paradigm
        # Random letters:
        if self.params.letter_set=="Sloan":
            letters_possible=['C','D','H','K','N','O','R','S','V','Z']
            #letters_possible=['O','C','P','R','S','N','H','W','M','Z']
            #letters_possible=['C','C','S','S','P','P','R','R','O','O']
        elif self.params.letter_set=="custom":
            letters_possible=self.params.letter_set_custom #['C','S','O','P','R']
        elif self.params.letter_set=="Ts":
            letters_possible=Ts_chars
        else:
            letters_possible=[chr(c) for c in np.arange( ord('A'),ord('Z')+1 )]

        if (self.params.target_sequence=="balanced") and self.params.char_target_rand:
            target_let=letters_possible[ self.params.target_seq[trialNum]%10 ] # TODO: only works <10 set
            untarget_let=letters_possible[ self.params.target_seq[trialNum]/10 ]
        elif self.params.char_target_rand:
            target_let=letters_possible[np.random.randint( len(letters_possible) )]
            # Pick a different letter for untarget:
            untarget_let=target_let
            while( untarget_let==target_let):
                untarget_let=letters_possible[np.random.randint( len(letters_possible) )]
        elif (self.params.char_target=="flip"):
            target_let=self.params.char_untarget
            untarget_let=self.params.char_untarget
        elif (self.params.char_untarget=="flip"):
            target_let=self.params.char_target
            untarget_let=self.params.char_target
        else:
            target_let=self.params.char_target
            untarget_let=self.params.char_untarget
        self.target_let=target_let
        self.untarget_let=untarget_let

        (target_element,self.target_which)=oddball_select(self.params.num_spokes)
        for nangle,atarg in enumerate(self._stims):
            astim=atarg._stim
            astim.ori = self.params.ori #np.random.randint(0,360+1 ) #180
            if self.params.variable=="size":
                astim.height = val #np.random.randint(0,360+1 ) #180

            if nangle==target_element:
                if self.params.letter_set=="Ts":
                    astim.setText( self.params.char_target )
                    astim.ori=Ts_ori_map[target_let]
                else:
                    astim.setText( target_let )
                    if (self.params.char_target=="flip"):
                        astim.ori += 180
            else:
                if self.params.letter_set=="Ts":
                    astim.setText( self.params.char_target )
                    astim.ori=Ts_ori_map[untarget_let]
                else:
                    astim.setText( untarget_let )
                    if (self.params.char_untarget=="flip"):
                        astim.ori += 180

        if self.params.variable=="size":
            if self.params.foveal_helper:
                self._helper._stim.setHeight(val) 

        if self.params.foveal_helper==True:
            if self.params.helper_identity_flanker:
                self._helper._stim.setText('X')
            elif self.params.helper_identity_uninformative:
                uninf=target_let
                while (uninf==target_let) or (uninf==untarget_let):
                    uninf=np.random.permutation(letters_possible)[0]
                self._helper._stim.setText(uninf)
            else: # normal case: cue target
                if self.params.letter_set=="Ts":
                    self._helper._stim.setText( 'T' )
                    self._helper._stim.ori=Ts_ori_map[target_let]
                else:
                    self._helper._stim.setText(target_let)
            if self.params.helper_polarity_reverse:
                self._helper._stim.color=(-1,-1,-1)
            if self.params.helper_lowercase:
                self._helper._stim.setText( self._helper._stim.text.lower() )
            self._helper._stim.height = val
            if not self.params.letter_set=="Ts":
                self._helper._stim.ori = self.params.helper_orientation 

        # UNTESTED:
        if self.params.bar_flankers:
            [aflank.update(siz, spac=bar_flanker_spacing) for aflank in flankers_all]

    def draw(self):
        [astim.draw() for astim in self._stims]
        if (self.params.foveal_helper==True) and (self.params.helper_trial):
            self._helper.draw() 

class FlankedRings(StimSet):
    def __init__(self, name, win, params):
        self.params=params
        self.targets = OddballLetterRing( name="targets", win=win, params=params, radius=(self.params.radius ) )
        if self.params.flankers:
            if self.params.flankers_theta:
                self.flankers_i = LetterRing( name="flankers_i", win=win, params=params, spokes=self.params.flanker_spokes,
                    text='I', radius=(self.params.radius-self.params.flanker_spacing ), jitter_amt=0, color=self.params.color_flanker )
                self.flankers_i2 = LetterRing( name="flankers_i2", win=win, params=params, spokes=self.params.flanker_spokes,
                    text='f', radius=(self.params.radius-self.params.flanker_spacing ), jitter_amt=0, color=self.params.color_flanker )
                self.flankers_o = LetterRing( name="flankers_o", win=win, params=params, spokes=self.params.flanker_spokes,
                    text='H', radius=(self.params.radius+self.params.flanker_spacing ), jitter_amt=0, color=self.params.color_flanker )
                self.flankers_o2 = LetterRing( name="flankers_o2", win=win, params=params, spokes=self.params.flanker_spokes,
                    text='e', radius=(self.params.radius+self.params.flanker_spacing ), jitter_amt=0, color=self.params.color_flanker )
                self._stims = [self.targets,self.flankers_i,self.flankers_o,self.flankers_i2,self.flankers_o2]
                self.flankers = [self.flankers_i,self.flankers_o,self.flankers_i2,self.flankers_o2]
            else:
                self.flankers_i = LetterRing( name="flankers_i", win=win, params=params, spokes=self.params.flanker_spokes,
                    text=self.params.char_flanker_i, radius=(self.params.radius-self.params.flanker_spacing ), jitter_amt=self.params.jitter_amt_flanker, color=self.params.color_flanker )
                self.flankers_o = LetterRing( name="flankers_o", win=win, params=params, spokes=self.params.flanker_spokes,
                    text=self.params.char_flanker_o, radius=(self.params.radius+self.params.flanker_spacing ), jitter_amt=self.params.jitter_amt_flanker, color=self.params.color_flanker )
                #self.flankers_i2=[]
                #self.flankers_o2=[]
                self._stims = [self.targets,self.flankers_i,self.flankers_o]
                self.flankers = [self.flankers_i,self.flankers_o]
        else:
            self._stims = [self.targets]
    def update(self, val, trialNum):
        flankers_theta=[ ['I','f'], ['H','e'] ]  # vertical, horizontal
        [astim.update(val, trialNum) for astim in self._stims]
        self.target_which=self.targets.target_which # propogate from child; TODO: improve!!
        self.target_let=self.targets.target_let
        self.untarget_let=self.targets.untarget_let
        if self.params.flanker_texture:
            [aflanker._stim.setText(self.untarget_let) for aflanker in self.flankers_i._stims]
            [aflanker._stim.setText(self.untarget_let) for aflanker in self.flankers_o._stims]
        if self.params.flankers_alternate:
            [self.flankers_i._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
            [self.flankers_o._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
        if self.params.targets_alternate:
            [self.targets._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
        if self.params.char_flanker_i=='\\':
            slashes=[ '\\', '/' ]
            [self.flankers_i._stims[n]._stim.setText( slashes[np.random.randint(0,2)] ) for n in np.arange(self.params.num_spokes)]
        if self.params.do_spacings:
            dirs=[-1,-1,1,1]
            for nflanker,aflanker in enumerate(self.flankers):
                aflanker.regen( aflanker.win, aflanker.params, aflanker.center, 
                    aflanker.params.radius+dirs[nflanker]*aflanker.params.flankerseq[trialNum],
                    aflanker.spokes)

        if self.params.flankers and self.params.flankers_theta:
            rands_i=np.random.randint(0,2,size=self.params.flanker_spokes)
            rands_o=np.random.randint(0,2,size=self.params.flanker_spokes)
            for aflanker in self.flankers:
                [aflanker._stims[n]._stim.setText( flankers_theta[flip][0] ) for (n,flip) in enumerate(rands_i)]



class FlankerRings(StimSet):
    def __init__(self, name, win, params):
        self.params=params
        #self.targets = OddballLetterRing( name="targets", win=win, params=params, radius=(self.params.radius ) )
        if self.params.flankers:
            self.flankers_i = LetterRing( name="flankers_i", win=win, params=params, spokes=self.params.flanker_spokes,
                        text=self.params.char_flanker_i, radius=(self.params.radius-self.params.flanker_spacing ) )
            self.flankers_o = LetterRing( name="flankers_o", win=win, params=params, spokes=self.params.flanker_spokes,
                        text=self.params.char_flanker_i, radius=(self.params.radius+self.params.flanker_spacing ) )
            self._stims = [self.flankers_i,self.flankers_o]
        else:
            self._stims = [ ]
    def update(self, val, trialNum):
        [astim.update(val, trialNum) for astim in self._stims]
        #if self.params.flanker_texture:
            #untarget_let=self.targets.untarget_let
            #[aflanker._stim.setText(untarget_let) for aflanker in self.flankers_i._stims]
            #[aflanker._stim.setText(untarget_let) for aflanker in self.flankers_o._stims]
        #if self.params.flankers_alternate:
            #[self.flankers_i._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
            #[self.flankers_o._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]
        #if self.params.targets_alternate:
            #[self.targets._stims[n]._stim.setText(" ") for n in np.arange(1,self.params.num_spokes,2)]

class TwoLines(Stim):
    def __init__(self, win, name, pos, size, lw=2):
        # Prepend with "_" non-serialized things
        if not(win==None):
            self.win=win
            self._stims=[
                visual.Line( win, start=pos, end=pos, lw=lw ),
                visual.Line( win, start=pos, end=pos, lw=lw )
            ]
        else:
            self.win=None
            self._stim=None

        self.name=name
        self.font=font
        self.pos=pos
        self.size=size
        self.lw=lw
        return
    def clone(self):
        anew=TwoLines(self.win, '%sX'%self.name, self.pos, self.size, self.lw)
        return anew
    def __repr__(self):
        rep1='%g, %g,'%(self._stims[0].pos[0], self._stims[0].pos[1])
        rep2='%g, %g,'%(self._stims[1].pos[0], self._stims[1].pos[1])
        return '%s, %s,'%(rep1, rep2)
    # For each trial, set up stuff
    def update(self):
        #[astim.update(val) for astim in self._stims]
        for astim in self._stims:
            xpos=np.random.randint(size)+self.pos[0]
            ypos=np.random.randint(size)+self.pos[1]
            astim.pos=(xpos,ypos)

class Dots(StimSet):
    def __init__(self, win, params, pos, sqsize ):
        stimUL=visual.Circle( win, pos=(pos[0]-sqsize,pos[1]-sqsize), units='pix', fillColor=params.color_target, radius=3 )
        stimUR=visual.Circle( win, pos=(pos[0]-sqsize,pos[1]+sqsize), units='pix', fillColor=params.color_target, radius=3 )
        stimBL=visual.Circle( win, pos=(pos[0]+sqsize,pos[1]-sqsize), units='pix', fillColor=params.color_target, radius=3 )
        stimBR=visual.Circle( win, pos=(pos[0]+sqsize,pos[1]+sqsize), units='pix', fillColor=params.color_target, radius=3 )
        self._stims = [stimUL,stimUR,stimBL,stimBR]

class DotsRing(StimSet):
    def __init__(self,name="",win=None, params=None, sqsize=None, center=(0,0), radius=None):
        self.params=params
        if radius==None:
            radius=self.params.radius
        if sqsize==None:
            sqsize=self.params.sqsize
        self._stims=[Dots(win, params, pos=apos, sqsize=sqsize) for (npos,apos) in
            enumerate(make_ring(center, radius, self.params.num_spokes, phase=self.params.phase, extent=self.params.ring_extent)) ] 

################################################
# New Two lines
################################################
# Ts. Normal orientation, then rotations (clockwise)
line2_Ts=[ [[0,2],[1,7]], [[2,8],[3,5]], [[8,6],[7,1]], [[6,0],[3,5]] ]
line2_Ls=[ [[0,2],[0,6]], [[0,2],[2,8]], [[2,8],[8,6]], [[8,6],[6,0]] ]
line2_Ss=[ [[0,8],[1,7]], [[2,6],[1,7]], [[0,8],[3,5]], [[2,6],[3,5]] ] # "scissors"
#line2_Xs=[ [[0,8],[2,6]], [[1,7],[3,5]] ] 
line2_Vs=[ [[0,7],[7,2]], [[2,3],[3,8]], [[6,1],[1,8]], [[5,0],[5,6]] ] 
line2_V2s=[ [[2,8],[8,0]], [[2,8],[2,6]], [[0,6],[0,8]], [[0,6],[2,6]] ] 
# X,Y offsets of each point index
#offsets =  [[-1,-1],[0,-1],[1,-1], [-1,0],[0,0],[1,0], [-1,1],[0,1],[1,1]]

pts3=[-1,0,1]
pts3_x,pts3_y=np.meshgrid( pts3,pts3)
pts4=[-1,-0.33,0.33,1]
pts4_x,pts4_y=np.meshgrid( pts4,pts4)
pts5=[-1,-0.5,0,0.5,1]
pts5_x,pts5_y=np.meshgrid( pts5,pts5)

offsets = zip( pts4_x.ravel(), pts4_y.ravel() )

def buildpairseq(stims, shuffle=True):
    Nstims=len(stims)
    vals= [(stims[x],stims[y]) for y in xrange(0,Nstims) for x in xrange(0,Nstims) if not(x==y) ]
    if shuffle:
        np.random.shuffle(vals) # modifies in-place
    return vals

class Pairseq(object): 
    def __init__(self, Nstim, shuffle=True ):
        self.seq=buildpairseq(Nstim, shuffle)
        self.itr=iter( self.seq )
    def next(self):
        return self.itr.next()

def makerel2(center, radius, pts):
    pts_rel=[ (center[0]+offsets[apt][0]*radius, center[1]+offsets[apt][1]*radius ) for apt in pts]
    return np.reshape( pts_rel, (len(pts_rel)/2,2,2) )

def getlines(center, radius, (lines1,lines2)):
    # for different types of stimuli, return endpts of two lines
    #lines1,lines2=all2s[which]
    return makerel2( center, radius, [lines1[0], lines1[1], lines2[0], lines2[1] ] )

def verts2let(verts):
    s=[pos2let(apos) for apos in verts]
    return s

def pos2let(apos):
    return chr(ord('a') + apos)

def code2points(code):
    points=[ord(apoint)-ord('a') for apoint in code]
    return points


class TwoLines2(StimSet):
    def __init__(self, win, name, pos, size, lineWidth=2, edges=2, color='white', colorize=False ):
        # Prepend with "_" non-serialized things
        if not(win==None):
            self.win=win
                # TODO: pull linewidth from params passed in??
                # TODO: fudge for size. /5.0 too fat, /10 maybe too skinny
            if colorize:
                self._stims=[visual.Line( win, start=pos, end=pos, lineWidth=size/8.0, lineColor=colors_segment[edge] ) for edge in range(edges)]
            else:
                self._stims=[visual.Line( win, start=pos, end=pos, lineWidth=size/8.0, lineColor=color ) for edge in range(edges)]
        else:
            self.win=None
            self._stim=None

        self.name=name
        self.pos=pos
        self.size=size
        self.lineWidth=lineWidth
        self.edges=edges
        self.colorize=colorize
        return

    def generate(self):
        self.points=np.random.permutation( np.arange(len(offsets)) )[0:len(self._stims)*2]
        self.code=''.join( [pos2let(apoint) for apoint in self.points] )
        return self.code

    def __repr__(self):
        return self.code

    # For each trial, set up stuff
    def update(self, code=None):
        if not (code is None): 
            self.code=code
            self.points=code2points(self.code)
            pos=makerel2(self.pos,self.size,self.points)
        else:
            # "offsets" indicates each possible point (x,y pair). 

            #print points
            #points=np.concatenate( (points, points[-1] ) )
            pos=makerel2(self.pos,self.size,self.points)
        #print newlines, pos
        for nstim,astim in enumerate(self._stims):
            astim.start=pos[nstim][0]
            astim.end  =pos[nstim][1]

        #self.target_let=verts2let(self.points)
        #self.untarget_let=verts2let(self.points)

def lines_intersect(A,B,C,D):
    # tell if the lines from A to B and C to D intersect.
    # from http://stackoverflow.com/questions/563198/how-do-you-detect-where-two-line-segments-intersect
    #E=np.array([offsets[B][0]-offsets[A][0],offsets[B][1]-offsets[A][1]])
    #F=np.array([offsets[D][0]-offsets[C][0],offsets[D][1]-offsets[C][1]])
    #E=np.array([B[0]-A[0],B[1]-A[1]])
    #F=np.array([D[0]-C[0],D[1]-C[1]])
    E=B-A
    F=D-C
    P=np.array([ -E[1], E[0] ])
    h=np.dot(A-C,P) / np.dot(F,P)
    return h

def test_point_line(p0,p1,p2):
    # From Wikipedia https://en.wikipedia.org/wiki/Distance_from_a_point_to_a_line
    d=(p2[1]-p1[1])*p0[0]-(p2[0]-p1[0])*p0[1]+p2[0]*p1[1]-p2[1]*p1[0]
    if abs(d)<np.finfo('float').resolution:
        d=0
    return d

def lines_intersect(a,b,c,d):
    A=offsets[a]
    B=offsets[b]
    C=offsets[c]
    D=offsets[d]

    d0=test_point_line(A, C, D)
    d1=test_point_line(B, C, D)
    d2=test_point_line(C, A, B)
    d3=test_point_line(D, A, B)
    signs=np.sign([d0, d1, d2, d3] )
        
    if (signs[0]!=signs[1]) and (signs[2]!=signs[3]):
        intersect=True
    else:
        intersect=False
    return intersect

class TwoLinesRepeats(TwoLines2):
    def generate(self):
        self.points=np.random.randint(0,len(offsets),size=len(self._stims)*2 )
        self.code=''.join( [pos2let(apoint) for apoint in self.points] )
        return self.code

# backbone type: all lines must touch the 'first' line
class TwoLinesConnected(TwoLines2):
    def generate(self):
        self.points=np.random.permutation( np.arange(len(offsets)) )[0:len(self._stims)*2] # fill, but will only retain first 2
        for npt in np.arange(1,self.edges):
            points=np.random.permutation( np.arange(len(offsets)) )[0:2] # pick next two points
            while lines_intersect(self.points[0], self.points[1], points[0], points[1] )==False:
                points=np.random.permutation( np.arange(len(offsets)) )[0:2] # pick next two points
            self.points[npt*2:(npt*2+2)]=points
        self.code=''.join( [pos2let(apoint) for apoint in self.points] )
        return self.code

# snake type: all lines must touch the previous line
class TwoLinesConnected2(TwoLines2):
    def generate(self):
        self.points=np.random.permutation( np.arange(len(offsets)) )[0:len(self._stims)*2] # fill, but will only retain first 2
        for npt in np.arange(1,self.edges):
            points=np.random.permutation( np.arange(len(offsets)) )[0:2] # pick next two points
            while lines_intersect(self.points[npt*2-2], self.points[npt*2-1], points[0], points[1] )==False:
                points=np.random.permutation( np.arange(len(offsets)) )[0:2] # pick next two points
            self.points[npt*2:(npt*2+2)]=points
        self.code=''.join( [pos2let(apoint) for apoint in self.points] )
        return self.code

class GenericSeqRing(StimSet):
    def __init__(self,name,win, params, stimclass, stimparams, trialseq, radius=None, center=(0,0), jitter_amt=0 ):
        self.params=params
        if radius==None:
            radius=self.params.radius
        try:
            del stimparams['pos'] # just in case
        except:
            pass
        self._stims=[ stimclass(pos=apos, **stimparams) for (npos,apos) in enumerate(make_ring(center, radius, self.params.num_spokes, phase=self.params.phase, jitter_amt=jitter_amt, extent=self.params.ring_extent)) ] 
        self.trialseq=trialseq

    def update(self,val,trialNum):
        (vals1,vals2)=self.trialseq.itr.next()
        (self.target_element,self.target_which)=oddball_select(self.params.num_spokes)
        # these are used for logging:
        #self.target_let=vals1[1]*10+vals1[0]
        #self.untarget_let=vals2[1]*10+vals2[0]
        self.target_let=self._stims[0].generate() # make target code (will be overwritten in loop)
        self.untarget_let=self._stims[0].generate() # make untarget code  (will be overwritten in loop)

        # TODO: conflicts with vals1 and vals2 above which are thrown out
        vals1=self.target_let
        vals2=self.untarget_let
        for nstim,astim in enumerate(self._stims):
            if nstim==self.target_element:
                astim.update(vals1)
            else:
                astim.update(vals2)
        # TODO: adjust nominal spacing of flankers.. should not be here I think though.
        #if self.params.variable=="size":
            #[aflanker.setHeight(val) for astim in self._stims]
            #[aflanker.setHeight(val) for astim in self._stims]


def Line2Ring(win, params):
    allseq=np.concatenate( [line2_Ts, line2_Ls, line2_Ss, line2_Vs, line2_V2s] )
    if params.target_sequence=="blocked":
        shuffle=False
    else:
        shuffle=True
    trials=Pairseq(allseq, shuffle=shuffle)
    stimparams={'win':win, 'name':None, 'size':params.size/2.0, 'lineWidth':params.size/5.0, 'edges':params.edges }
    theRing=GenericSeqRing( "line2ring", win, params, TwoLinesConnected, stimparams, trials, radius=params.radius, jitter_amt=10.0)
    return (theRing)

# Target = Line2Ring
# Flankers4 =

class flankers(StimSet):
    def __init__(self,win,flankerof,stimclass,params,locations='4'):
        self.flankerof=flankerof # just who am I flanking, anyway?!
        self.params=params
        #self.flanker.of.pos - params.

        stimparams_letter={'win':win, 'name':"", 'font': 'Sloan', 'size':params.size,
            'text':'O', 'ori':0, 'color':params.color_flanker, 'params':params }

        if locations=='4':
            offsets=np.array(zip( [-params.flanker_spacing,0,params.flanker_spacing,0],
                [0,-params.flanker_spacing,0,params.flanker_spacing] ) )
        if locations=='4p':
            r=params.flanker_spacing
            thetas=np.array([np.pi,np.pi/2.0,0,-np.pi/2.0])+params.phase
            offsets=np.array(zip( np.cos(thetas), np.sin(thetas) ) ) * r
        elif locations=='2r':
            r=params.flanker_spacing
            thetas=np.array([np.pi,0.0])+params.phase
            offsets=np.array(zip( np.cos(thetas), np.sin(thetas) ) ) * r
        
        self._stims=[stimclass(pos=self.flankerof.pos+loc_rel, **stimparams_letter) for nwhich,loc_rel in enumerate(offsets) ]
        
