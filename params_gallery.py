from numpy import pi,array

fullscr = True
screendim = [1024,768]
#screendim = [1280,1024]
screen_height_cm = 30 # in cm
distance_cm=40
ntrials = 80 
color_target=(1,1,1)
color_flanker=(1,1,1)
size_noise=1024
ppd=26.3 # pix_per_deg

# Trial control
mode="erase"
method="hybrid" # "cs|hybrid|staircase"
variable="duration" # size|duration|spacing
#variable_type="absolute" # absolute|nominal
cs_steps=[-2,-1,0,1,2] #[-20,-10,0,10,20]
cs_center=0.02

# Times in seconds:
trial_time =  0.150 #180 
pre_time = 0.100
pre2_time = 0.000 # time between helper preview and trial
mask_time = 0.1
between_time = 0.000
#noise_on_time=0.100 #TODO: I think not used anymore (1/2017)

# Fixation
fixation_size_deg=0.25
# Specifics for cross-type fixation:
fixation_char='+'
fixation_size=ppd * 2.14 * fixation_size_deg# x2.14 to make plus entire size of char
fixation_pos=(0,+fixation_size/20.0) # to make cross actually in middle 

# Ring parameters:
num_spokes=8
radius_deg=10.0
radius=ppd*radius_deg
angles=None
size=2.00*ppd 

ring_extent=1.00 #.25 # proportion of circle fill up. If <1, includes endpoints
phase=pi/4.0 #pi/4.0 #-pi*ring_extent #pi/2.0-pi*ring_extent # so extent is centered around pi/2 (up)

letter_set = "Ts" # Sloan|custom|Ts
# Go in the clockwise direction (from 0) if you want to do identification with a custom set:
letter_set_custom = ['C', 'S', 'P', 'R'] #['F', 'P', 'R', 'B'] # used if letter_set=="custom" and char_target_rand==True
char_target_rand=True
char_target='T'
char_untarget='<'
font='Sloan'
ori=0
flanker_spokes=8
jitter_amt_deg=0.2
jitter_amt=ppd*jitter_amt_deg # affects all LetterRings
jitter_amt_flanker=0.0

# Flankers
flankers=True
flankers4=False
flankers_theta=True
flanker_spacing_nominal=False # If false, pixels, if True, is "X" of size
char_flanker_i ='X'
char_flanker_o='X'
target_sequence="balanced"   # "blocked"
orientation = 0 # orientation
flanker_texture=False # Make flanker match untarget
flankers_alternate=False # don't flank everything
flankers_random=True 
targets_alternate=False # don't flank everything
bar_style="None" # None|box|bars|bars2
bar_sep=5.0 # edge-edge, in multiples of bar width, 0=adjacent


# Helper
foveal_helper=False
helper_pre=False
helper_trial=True
helper_orientation = 0 # matches orientation of targets
helper_identity_flanker=False
helper_identity_uninformative=False # uninformative helper
helper_polarity_reverse=False
helper_lowercase=False
helper_font='Sloan' #'Ergonome'

# To remove:
bar_flankers=False # Bars aren't really tested..

method="hybrid"
do_stair = False
hybrid=True
test_flanker_spacing=False
test_duration=True
cs=True
cs_center=40
cs_steps=[0] #[-2,-1,0,1,2] #[-20,-10,0,10,20]
test_flanker_spacing=False
test_duration=False
test_size=False
hybrid=False
size_fixed=20

lineWidth=8 # for two-line

# --- Params for our first experiment:
# By default, doing a CS at a single setting, unflanked
flanker_spacing= size*100.2 #1050.0  # 60 is far away
c1_dist=2.2*ppd
c2_dist=3.0*ppd
c3_dist=5*ppd
cc_dist=c2_dist

size_cs_logspace=(10,60,5)

do_spacings=True
spacings_cs = array([5000/ppd, 4, 2, 1, 1.5])*ppd
spacings_blocked = False

# Change the things below for each subject/condition
# --------------------------------------------
# Conditions:
#   0==calibration for size reading
#   1==calibration for size reading, crowded
#   0H==calibration for size reading, with helper
#   C==Crowded near
#   CC==Crowded medium
#   CCC==Crowded far
#   U=Uncrowded
#   HC=Crowded (2), with helper
#   HCU=Crowded (2), With uninformative helper
#   HCF=Crowded (2), With flanker identity
#   H=Uncrowded, helper
#   HU=Uncrowded, uninformative helper
#   S=Sizes, constant stimuli

#   # targets, # flankers
#   16T16=Texture: 16X16, matching
#   16X16=16 spokes, 16 X flankers
#   16X8=16 spokes, 8 X flankers
#   16T8=16 spokes, 8 texture flankers
#   8T16=16 spokes, 8 texture flankers
#   8T8=16 spokes, 8 texture flankers
#   8X8=8 spokes, 8 X flankers
#   16T16C=Texture: 16X16, matching, closer-spacing

SubjectName = 'Xdc'
#condition="2L-4C"
#condition="16X16"
#condition="none-10-200"
#cs_center=30
flanker_spacing=5000  #c3_dist
flanker_spacing=size*5.0
#flanker_spacing=5000  #c3_dist

#foveal_helper=True
# To test wih a singleton
num_spokes=4
flanker_spokes=0
response='direction' #direction' # id|direction
paradigm="direction"

edges=2

condition="c1of2n0"
