import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core
import psychopy.data as pd 
import numpy as np
import time
import matplotlib.pyplot as pyplot
import util
import importlib
import datetime
import copy

import stims

from adjust_params import *

def endpoints_arc( pos, length, ori ):
    # length/2.0 is radius
    xs=np.array([ length/2.0*np.cos(ori1) for ori1 in np.linspace(np.pi/2.0,3*np.pi/2.0,arc_num_points,endpoint=True)-ori] )
    ys=np.array([-length/2.0*np.sin(ori1) for ori1 in np.linspace(np.pi/2.0,3*np.pi/2.0,arc_num_points,endpoint=True)-ori] )
    points=zip( pos[0]+xs, pos[1]+ys )
    return points

def calc_endpoints( pos, length, ori ):
    offset1=np.array( [length/2.0*np.cos(ori), length/2.0*np.sin(ori) ] )
    points=np.array([pos+offset1,pos-offset1])
    return points

class Line(object):
    def __init__(self, myWin, idcode, params_line, params_widget, outfile=None, time_start=None, colorize=False):
        self.params=copy.copy(params_line)
        self.idcode=idcode
        self._stim=visual.Line(myWin, **params_widget)
        verts=calc_endpoints(**self.params)
        self._stim.vertices=verts
        self.outfile=outfile
        self.time_start=time_start
        self.deleted=False
        self.colorize=colorize

    def draw(self):
        if self.colorize:
            self._stim.lineColor=stims.colors_segment[ self.idcode % 9 ]
        verts=calc_endpoints(**self.params)
        self._stim.vertices=verts
        if self.deleted==False:
            self._stim.draw()

    def param_string(self):
        s="%s,%d,%d,%d,%0.6f,%d"%("Line",self.idcode,self.params['pos'][0], self.params['pos'][1],
            self.params['ori'], self.params['length'] )
        return s

    def nudge( self, param, val ):
        val1=0
        if param=='length':
            self.params['length'] += val
            if self.params['length']<0:
                self.params['length']=0
        elif param=='ori':
            self.params['ori'] += val
        elif param=='pos':
            self.params['pos'] += np.array(val)
            val1=val[1]
            val=val[0]
            if self.params['pos'][0]>limits_x[1]:
                self.params['pos']=(limits_x[1],self.params['pos'][1])
            elif self.params['pos'][0]<limits_x[0]:
                self.params['pos']=(limits_x[0],self.params['pos'][1])
        elif param=='del':
            self.deleted=True

        if not( self.time_start is None):
            time_delta=datetime.datetime.now()-self.time_start

        if not( self.outfile is None):
            self.outfile.write("%d.%d,%s,%0.6f,%0.6f,%s\n"%(time_delta.seconds,time_delta.microseconds,param,val,val1,self.param_string()) )

class Arc(Line):
    def draw(self):
        if self.colorize:
            self._stim.lineColor=stims.colors_segment[ self.idcode % 9 ]
 
        verts=endpoints_arc(**self.params)
        self._stim.vertices=verts
        if self.deleted==False:
            self._stim.draw()

    def param_string(self):
        s="%s,%d,%d,%d,%0.6f,%d"%("Arc",self.idcode,self.params['pos'][0], self.params['pos'][1],
            self.params['ori'], self.params['length'] )
        return s

def stim_factory(which, *params):
    if which=='Arc':
        creator=Arc
    elif which=='Line':
        creator=Line
    newstim=creator(*params)
    return newstim

    
