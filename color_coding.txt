Rubric

Black -> new segments
Two lines get the same colour, if one target-line got split
Not sure = Not sure! = 1-2 segments ambiguous 
Not sure!! = no idea


LN: results/Sx_colors_04272017-00.csv
NC: results/Sx_colors_04212017-00.csv


Often, two lines are brought together into one and then you have to decide which one you colour�
Certain targets lead to the same mistakes: e.g. 04272017-84.csv or 04272017-125.csv -> tricky to match� (handlebars?)
If I assume a drawing is done in the peripheral space I can understand certain mistakes better (easier to match the colours then)�
