# Call replay on all the files in a directory to generate images.
# i.e., bash auto_replay.sh letters/ln*.csv

FLANKER_TYPE=2 # for more recent ones, X or less

for f in $@
    do python replay.py --flankers $FLANKER_TYPE --debug --speed -1 --movie --nowait $f;
done
