import psychopy
import psychopy.visual as visual
import psychopy.event as event
import psychopy.core as core
import psychopy.data as pd 
import numpy as np
import time
import matplotlib.pyplot as pyplot
import util
import importlib
import datetime
import copy
import logging

import stims

from adjust_params import * # bad practice to include all, but...
import adjust_params as params # also do it the more principled day
from adjust_stims import Arc,Line,stim_factory

if params.enable_sounds:
#psychopy.prefs.general['audioLib'] = ['pygame'] # not pyo, which seems problematic on Windows
    from psychopy import sound

OUTFILE_VERSION=1.0 # so replay knows about this type of result file

# Current state of UI:
STATE_MOVING=0      # normal mode to move or change length
STATE_ROTATING=1    # in a rotation
STATE_MENU=2        # in the pop-up add, etc. menu

# Button states: (to implement hold and manage mouse releases, etc.)
BTN_STATE_NOPRESS=0
BTN_STATE_PRESS=1 

MAX_TIMER=9999 
# set the timers to this value to make them "off"
# If we get to time=MAX_TIMER it will trigger an undesired action !

# Note this is not an OO function, I am just calling first param "self" since
# This could eventually go in the adjust expt. class
def add1(self):
    menu_add_types={1:"Line",2:"Arc"}
    new=stim_factory(menu_add_types[self.add_which],
        self.win, self.num_matches, params_line, params_widget_line,
        self.outfile, self.time_start, self.colorize)

    new.params['pos'] = (match_pos_x_deg*ppd,new.params['pos'][1])
    self.num_matches += 1
    self.matches += [new]
    self.select_which=self.num_matches-1 # so when we come out, pointed to new target

    self.state_current=STATE_MOVING
    self.state_button=0
    self.timer_blink.reset(blink_duration) 
    self.timer_mute.reset(mute_duration)

    if params.enable_sounds:
        self.sound_add.play()
    log_msg(self, "add", self.add_which, self.num_matches)

def log_msg(self, what, val, val1=0):
    time_delta=datetime.datetime.now()-self.time_start
    self.outfile.write("%d.%d,%s,%0.6f,%0.6f\n"%(time_delta.seconds,time_delta.microseconds,what,val,val1 ) )

class adjust_experiment(object):

    state_current=STATE_MOVING
    state_button=0
    select_which=0
    add_which=0 # selection screen which selected to add
    add_pos=0 # selection screen which selected to add

    if params.enable_sounds:
        sound_add=sound.Sound("add_sparkle.wav")
        sound_done=sound.Sound("win_tada.wav")
        sound_del=sound.Sound("pluck.wav")
        sound_buzz=sound.Sound("buzz.wav")

    def cycle_next(self):
        new_target=self.select_which+1
        while True: 
            if new_target==self.num_matches:
                if cycle_to_menu:
                    break
                else:
                    new_target=0
            stim_next = self.matches[new_target]
            if stim_next.deleted==False:
                break # got one, break out of while loop
            new_target += 1

        # Will get here if cycle_to_menu is true:
        if new_target==self.num_matches: 
            self.state_current=STATE_MENU
            self.select_which=0
            self.add_which=0
        else: # normal selection of new target
            self.select_which=new_target
            self.timer_blink.reset(blink_duration)
            log_msg(self, "sel", new_target)

    def setup(self, win=None, default_stims=True, rand_lines=None, bare=False):
        if win==None:
            win = visual.Window(**params_win)
            self.win_mine=True
        else:
            self.win_mine=False
        self.win=win

        self.win.setMouseVisible(False)
        self.fixation = visual.TextStim(win, alignHoriz='center', alignVert='center',**params_fixation)
        self.fixation2 = visual.TextStim(win, alignHoriz='center', alignVert='center',**params_fixation2)
        if params.allow_stim_view==False:
            self.fixation2.color=params.params_win['rgb']

        outfilename = util.get_unique_filename("results/%s_%s_%s-%%02d.csv" % (subject_name, condition,
                    time.strftime("%m%d%Y", time.localtime() ) ) )
        outfile = open(outfilename, "wt")
        self.outfile=outfile

        self.time_start=datetime.datetime.now() # for the overall time sequence stored in file

        params_line['pos']=[ppd*target_pos_x_deg,0]

        self.mouse = event.Mouse()
        self.mouse.setPos( [0,0] )
        self.mouse.setVisible(False)
        log_msg(self,"ver",OUTFILE_VERSION)

        if default_stims:
            #target=Line(win, 100, params_line, params_widget_line, outfile=outfile, time_start=self.time_start)
            #target2=Line(win, 101, params_line, params_widget_line, outfile=outfile, time_start=self.time_start)
            target_pos = [target_pos_x_deg*ppd,params_line['pos'][1]]
            target=None

            # Three lines comprising the initial 3 edges in the match
            match=Line(win, 0, params_line, params_widget_line, outfile=outfile, time_start=self.time_start)
            match.params['pos'] = (match_pos_x_deg*ppd,target_pos[1])
            match2=Line(win, 1, params_line, params_widget_line, outfile=outfile, time_start=self.time_start)
            match2.params['pos'] = (match_pos_x_deg*ppd+5,target_pos[1])
            match3=Line(win, 2, params_line, params_widget_line, outfile=outfile, time_start=self.time_start)
            match3.params['pos'] = (match_pos_x_deg*ppd+10,target_pos[1])

            if bare:
                clutter=[]
            elif params.clutter_Xs:
                #print params.clutter_Xs, params.clutter_Xs==True, bare
                #Xs around the target and match
                clutter=[Line(win, 200+n, params_line, params_widget_line, outfile=outfile,time_start=self.time_start) for n in range(8)]
                clutter[0].params['pos'] = (target_pos[0] - flanker_spacing,target_pos[1] )
                clutter[1].params['pos'] = (target_pos[0] - flanker_spacing,target_pos[1] )
                clutter[2].params['pos'] = (target_pos[0] + flanker_spacing,target_pos[1] )
                clutter[3].params['pos'] = (target_pos[0] + flanker_spacing,target_pos[1] )
                clutter[0].params['ori'] = np.pi/4.0
                clutter[1].params['ori'] = 3*np.pi/4.0
                clutter[2].params['ori'] = np.pi/4.0
                clutter[3].params['ori'] = 3*np.pi/4.0

                clutter[4].params['pos'] = (match.params['pos'][0] - flanker_spacing,match.params['pos'][1] )
                clutter[5].params['pos'] = (match.params['pos'][0] - flanker_spacing,match.params['pos'][1] )
                clutter[6].params['pos'] = (match.params['pos'][0] + flanker_spacing,match.params['pos'][1] )
                clutter[7].params['pos'] = (match.params['pos'][0] + flanker_spacing,match.params['pos'][1] )
                clutter[4].params['ori'] = np.pi/4.0
                clutter[5].params['ori'] = 3*np.pi/4.0
                clutter[6].params['ori'] = np.pi/4.0
                clutter[7].params['ori'] = 3*np.pi/4.0
                for aclutter in clutter:
                    aclutter.params['length'] *= np.sqrt(2)
            elif params.clutter_lines:
                # Vertical lines around the target and match, and an extra "reference" line
                # Build and position clutter (around target and match)
                clutter=[Line(win, 10+n, params_line, params_widget_line, outfile=outfile,time_start=self.time_start) for n in range(5)]
                clutter[0].params['pos'] = (target.params['pos'][0] - flanker_spacing,target.params['pos'][1] )
                clutter[1].params['pos'] = (target.params['pos'][0] + flanker_spacing,target.params['pos'][1] )
                clutter[2].params['pos'] = (match.params['pos'][0] - flanker_spacing,match.params['pos'][1] )
                clutter[3].params['pos'] = (match.params['pos'][0] + flanker_spacing,match.params['pos'][1] )
            else:
                clutter=[]

            # Write out all the clutter information: (by faking an orientation nudge of zero)
            [aclutter.nudge('ori',0) for aclutter in clutter]

            if not (target==None):
                    # Jitter the target(s) (pos, ori, and length)
                    jitter_x=np.random.uniform(*pos_random_limits[0])
                    jitter_y=np.random.uniform(*pos_random_limits[1])
                    target.params['ori']=np.random.uniform(0.0,np.pi/2.0)
                    target.params['length']=np.random.uniform(*length_random_limits)
                    target.nudge('pos', [jitter_x,jitter_y] )
                    jitter_x=np.random.uniform(*pos_random_limits[0])
                    jitter_y=np.random.uniform(*pos_random_limits[1])
                    target2.params['ori']=np.random.uniform(0.0,np.pi/2.0)
                    target2.params['length']=np.random.uniform(*length_random_limits)
                    target2.nudge('pos', [jitter_x,jitter_y] )

            if bare:
                self.matches=[]
            elif params.clutter_Xs:
                self.matches=[match,match2,match3]
            elif params.clutter_lines:
                self.matches=[match,clutter[2],clutter[3],clutter[4]]
            else:
                self.matches=[match,match2,match3]

            if bare:
                self.targets=[]
                self.flankers=[]
            elif target_randstim:
                size=1.0*ppd
                lw=size/8.0
                if rand_lines==None:
                    rand_lines=params.rand_lines
                if params.target_code=="":
                    try:
                        edges=np.random.randint(rand_lines[0], rand_lines[1])
                    except TypeError:
                        edges=rand_lines
                else:
                    edges=len(target_code)/2
                target_posx=params.target_pos_x_deg*ppd
                randstim=stims.TwoLinesConnected(win=win, name='', pos=(target_posx,0), size=size, lineWidth=lw, edges=edges )#1.0*ppd/5.0, edges=edges )

                if params.target_code=="":
                    randstim.generate()
                    randstim.update()
                else:
                    randstim.update(params.target_code)
                self.targets=[randstim]
                log_msg(self, "lc:%s"%(randstim.code), params.target_pos_x_deg,edges )
                if flankers_randstim:
                    # TODO: Doesn't work!! (previous line can't be continued, these should be log_msgs
                    edges=3
                    inner=stims.TwoLinesConnected2(win=win, name='', pos=(target_pos_x_deg*ppd-flanker_spacing,0), size=size, lineWidth=lw, edges=edges )
                    outer=stims.TwoLinesConnected2(win=win, name='', pos=(target_pos_x_deg*ppd+flanker_spacing,0), size=size, lineWidth=lw, edges=edges )
                    inner.generate()
                    inner.update()
                    outer.generate()
                    outer.update()
                    self.outfile.write(",%s,%s\n"%(inner.code, outer.code) )

            if params.clutter_Xs:
                self.flankers=clutter 
            elif params.clutter_lines:
                self.targets=[target,target2]
                self.flankers=[self.clutter[0],self.clutter[1]]
            else:
                # randstim and clutter_Xs==False mean unflanked
                self.flankers=[] 

        else: # no default stims, start from blank
            self.matches=[]
            self.targets=[]
            self.clutter=[]

        self.num_matches=len(self.matches)

        # Eye-candy for the display:
        self.trash_can=visual.ImageStim(win, 'icons/Trash_Can-512.png', units='pix', pos=(0,-300), size=(100,100), contrast=-0.2)
        icon_size=(60,60)
        self.icons_top0=visual.ImageStim(win, 'icons/icon_back.png', units='pix',
            pos=(-100,300), size=icon_size, contrast=-0.1 )
        self.icons_top1=visual.ImageStim(win, 'icons/plus-circle-outline.png', units='pix',
            pos=(0,300), size=icon_size, contrast=-0.1 )
        self.icons_top2=visual.ImageStim(win, 'icons/checkmark_size2.png', units='pix',
            pos=(100,300), size=icon_size, contrast=-0.1 )

        self.icons_top3=visual.ImageStim(win, 'icons/icon_line.png', units='pix',
            pos=(-100,190), size=icon_size, contrast=-0.1 )
        self.icons_top4=visual.ImageStim(win, 'icons/icon_curve.png', units='pix',
            pos=(-20,190), size=icon_size, contrast=-0.1 )
        self.icons=[self.icons_top0,self.icons_top1,self.icons_top2,self.icons_top3,self.icons_top4]
        self.cursor_icons=[self.icons_top0,self.icons_top3,self.icons_top4,self.icons_top2]

        # Initialize timers
        self.timer_blink = core.CountdownTimer(blink_duration)
        self.timer_mute = core.CountdownTimer(blink_duration)
        self.timer_click_hold = core.CountdownTimer() # not active
        self.timer_buzz = core.CountdownTimer(5.0) # start 5 seconds in

        # gc_menu_stuff
        self.gc_menu_select=0
        self.gc_menu_closest=-1
        self.timer_gc_menu_hold = core.CountdownTimer(MAX_TIMER) # not active
        self.timer_gc_menu_mute = core.CountdownTimer(0) # <0 = not muted
        self.timer_gc_menu_enter = core.CountdownTimer(MAX_TIMER) 

    # Called at every frame. Complex state-machine following what subject is/was doing, possibly including gaze-contingent menu.
    def do_frame(self, drawall=True, doflip=True, gpos=None):

        done=False # exit after this trial?
        
        # Do Gaze Cont menu. If there is a position, gc menu is enabled, and we are not
        # muted, and they've held in the upper position for long enough
        if enable_gc_menu and not (gpos is None) :
            if (gpos[1]>params.gc_menu_yloc_min) and (self.timer_gc_menu_mute.getTime()<0
                    ) and (self.timer_gc_menu_enter.getTime()<0):
                self.state_current=STATE_MENU

                # Find spatially nearest button and select that one
                dists=np.array([((icon.pos[0]-gpos[0])**2+(icon.pos[1]-gpos[1])**2) for icon in self.cursor_icons])
                closest=np.arange(len(self.cursor_icons))[dists==np.min(dists)][0]

                if (closest==self.gc_menu_closest): # if being held on a valid item
                    if (self.timer_gc_menu_hold.getTime()<=0):
                        if closest==0: # return
                            self.state_current=STATE_MOVING
                            self.state_button=0
                            self.timer_blink.reset(blink_duration) 
                            self.timer_mute.reset(mute_duration)
                            self.timer_gc_menu_hold.reset(MAX_TIMER) 
                            self.select_which=0
                        elif closest==3:
                            done=True
                        else:
                           # self.timer_mute.reset(mute_duration*3.0) # so mute while held
                            self.timer_gc_menu_hold.reset(gc_menu_hold_duration)
                            self.timer_gc_menu_mute.reset(mute_duration*5.0)
                            self.add_which=closest
                            add1(self)

                    # else, do nothing at this time.. (until appropriate duration...)
                else:
                    # Reset the timer, will need to re-hold if position is lost
                    self.gc_menu_closest=closest
                    self.timer_gc_menu_hold.reset( gc_menu_hold_duration )

                self.add_which=closest
                self.gc_menu_select=closest
            elif (gpos[1]>params.gc_menu_yloc_min) and (self.timer_gc_menu_enter.getTime()>gc_menu_enter_duration):
                # eye in position (up) and first time seen recently (since timer would be MAX)
                # Set time to start enter.. Next time will skip this, since still counting down
                self.timer_gc_menu_enter.setTime(gc_menu_enter_duration)
            elif self.timer_click_hold.getTime()>0:
                # Do things if gaze is not on menu (so do some resetting of position, etc.)
                # IE, are moving eyes, potentially in gaze menu, but not anymore.
                #
                # However, if they are holding down the button, don't do anything here (i.e.,
                # assume okay to be in menu mode, etc.) and let the button logic override.

                # Reset the timer, will need to re-hold if position is lost
                self.gc_menu_closest==-1
                self.timer_gc_menu_hold.reset( gc_menu_hold_duration )

                if self.state_current==STATE_MENU: # exit out of menu
                    self.state_current=STATE_MOVING
                    self.state_button=0
                    self.timer_blink.reset(blink_duration*3.0) 
                    self.timer_mute.reset(mute_duration)
                    #self.select_which=0 #? I think this was causing new ones to not be selected
            else: # i.e., gaze not in menu, or not for long enough
                self.timer_gc_menu_enter.setTime(MAX_TIMER)

        stim_current = self.matches[self.select_which]
        extra_key=""
        if done==False:
            for key in event.getKeys():
                if key in [ 'escape' ]:
                    #core.quit()
                    done = True
                elif key=='o':
                    stim_current.nudge('length',-1)
                elif key=='l':
                    stim_current.nudge('length',1)
                elif key=='t':
                    stim_current.nudge('ori',-np.pi/200.)
                elif key=='r':
                    stim_current.nudge('ori',np.pi/200.)
                elif key=='left':
                    stim_current.nudge('pos',[-1,0])
                elif key=='right':
                    stim_current.nudge('pos', [1,0])
                elif key=='up':
                    stim_current.nudge('pos', [0,1])
                elif key=='down':
                    stim_current.nudge('pos',[0,-1])
                elif key=='space':
                    self.cycle_next()
                elif key=='c':
                    stim_current.idcode += 1
                    stim_current.idcode %= 9
                    log_msg(self, "color", stim_current.idcode)
                    stim_current.nudge('ori',0.)
                elif key=='d':
                    stim_current.nudge('del',0)
                    self.mouse.setPos([0,0]) 
                    if params.enable_sounds:
                        self.sound_del.play()
                elif key=='a':
                    self.add_which=1
                    add1(self)
                else:
                    extra_key=key

                # This doesn't work. But, it is sending 'lctrl' with a zoom gesture, but unsure how to process
                #elif key=='lctrl':
                    #inzoom=True
                    #val=mouse.getRel()
                    #if not( (val[0]==0) and (val[1] == 0)):
                        #target.nudge('ori', val[1]*np.pi/100.0 )

            if (self.state_current==STATE_MOVING) or (self.state_current==STATE_ROTATING):
                val=self.mouse.getRel()
                pos=self.mouse.getPos()
                if self.timer_mute.getTime()>0:
                    # The function of the "mute" timer is to prevent spurious movements after actions such as delete or
                    # add or origin snap reset. When "muted" the mouse position is read (previous two line), but it is
                    # ignored, since all the following logic is skipped by the if/elses... (Without this fix/hack
                    # mouse movement events were staying in some queue causing jumps.
                    pass
                elif reset_to_origin and (abs(pos[0]**2+pos[1]**2)>(400.0**2)) and (self.mouse.getPressed()[button_rotate]==0):
                    # if not rotating, and we get too far from the center of the screen, snap back to origin. Wait it necessary so that
                    # the jump is not processed like a mouse move
                    self.mouse.setPos([0,0]) 
                    self.timer_mute.reset(0.05)
                elif any( val ):
                    # Got mouse movement.
                    # Could check that is greater than some threshold (i.e., 2 or 3 pixels, to prevent small unwanted movements.)
                    if (self.mouse.getPressed()[button_rotate]==0):
                        # Button not pushed, so it's a normal edge position movement
                        stim_current.nudge('pos', val*scaler_pos )
                    else:
                        # Button pushed, so it's a rotation: add the (X,Y) relative/delta positions
                        stim_current.nudge('ori', (-val[0]+ val[1])*scaler_ori )
                        self.state_current=STATE_ROTATING
                    self.timer_click_hold.reset(MAX_TIMER) # indicate not in a hold
                    self.state_button = 0

                # Immediate click of button2 to goto menu: (currently disabled)
                #elif (self.mouse.getPressed()[button_menu]>0):
                    #self.state_current=STATE_MENU
                    #self.select_which=0
                    #self.add_which=0
                    #self.state_button=0

                else:
                    #no movement, but any clicks?
                    if (self.state_button & 1):
                        # Button was down before..
                        if (self.mouse.getPressed()[button_cycle]==0): # released
                            if self.state_current==STATE_ROTATING:
                                self.state_current=STATE_MOVING
                                self.timer_click_hold.reset(MAX_TIMER)
                            else: #Normal click
                                self.cycle_next()
                                self.state_button=0
                        else: # button was on and is being held 
                                t=self.timer_click_hold.getTime()
                                if t<0:
                                    self.timer_click_hold.reset(MAX_TIMER)
                                    self.state_current=STATE_MENU
                                    #self.select_which=0
                                    self.add_which=0
                                    self.state_button=1

                    else: # start press (was off, now on)
                        if (self.mouse.getPressed()[button_cycle]==1):
                            self.state_button = 1 
                            if (self.state_current!=STATE_ROTATING):
                                self.timer_click_hold.reset(hold_duration)

                # two-finger scroll (read as mousewheel message under linux and windows)
                val=self.mouse.getWheelRel()
                if not( (val[0]==0) and (val[1] == 0)):
                    stim_current.nudge('length', val[1]*scaler_length ) 

            elif (self.state_current==STATE_MENU):
                val=self.mouse.getRel()
                pos=self.mouse.getPos()

                if (abs(val[0])>0):
                    # Position of "imaginary" cursor of which action to do (or item to add)
                    self.add_pos += val[0] / 2.0
                    self.add_pos %= icon_select_maxval
                    self.add_which = int(self.add_pos/(icon_select_maxval/4)) # TODO: use # icons

                elif (self.state_button==1):
                    if (self.mouse.getPressed()[button_menu]==0): # release
                        if self.add_which==3:
                            done=True
                        elif self.add_which==0:
                            #if (self.state_current==STATE_MENU):
                            # already changing, so we are cycling
                            self.state_current=STATE_MOVING
                            self.state_button=0
                            self.timer_blink.reset(blink_duration) 
                            self.timer_mute.reset(mute_duration)
                            self.select_which=0
                        else: # ADD new
                            add1(self)
                else:
                    if (self.mouse.getPressed()[button_menu]==1):
                        self.state_button=1

            # Trash : could check if moving
            if (abs(stim_current.params['pos'][0])<20) and (stim_current.params['pos'][1]<-150): 

                # Find next valid
                new_target=self.select_which+1
                while new_target!=self.select_which:
                    if new_target==self.num_matches:
                       new_target=0 # wrap around to beginning
                    stim_next = self.matches[new_target]
                    if stim_next.deleted==False:
                        break # found one, break
                    new_target+=1

                if new_target==self.select_which: # wrapped all the way, so no valid left: prevent deletion
                    #self.state_current=STATE_MENU
                    pass
                else:
                    self.select_which=new_target
                    self.timer_blink.reset(blink_duration) 
                    self.timer_mute.reset(mute_duration)

                    stim_current.nudge('del',0)
                    self.mouse.setPos([0,0]) 
                    if params.enable_sounds:
                        self.sound_del.play()

            # Blink logic
            t=self.timer_blink.getTime()
            target_curr=self.matches[self.select_which]
            for atarget in self.matches:
                if (atarget==target_curr) and (self.timer_blink.getTime()>0):
                    # blink
                    atarget._stim.lineColor=[np.abs(np.sin(t*blink_rate))]*3 # color is gray/white from 0-1.0
                else:
                    atarget._stim.lineColor=params_widget_line['lineColor']

            self.fixation.draw()
            self.fixation2.draw()
            self.trash_can.draw()
            if self.state_current==STATE_MENU:
                self.icons_top0.contrast=contrast_icons_selected if (self.add_which==0) else contrast_icons_unselected1
                self.icons_top1.contrast=contrast_icons_selected if ((self.add_which<3) and (self.add_which>0)) else contrast_icons_unselected1
                self.icons_top3.contrast=contrast_icons_selected if (self.add_which==1) else contrast_icons_unselected2
                self.icons_top4.contrast=contrast_icons_selected if (self.add_which==2) else contrast_icons_unselected2
                self.icons_top2.contrast=contrast_icons_selected if (self.add_which==3) else contrast_icons_unselected2
            else:
                self.icons_top0.contrast=contrast_icons_dim 
                self.icons_top1.contrast=contrast_icons_dim
                self.icons_top2.contrast=contrast_icons_dim
                self.icons_top3.contrast=0
                self.icons_top4.contrast=0
                self.trash_can.contrast =contrast_icons_dim

            if drawall:
                draw_target=True
                draw_match=True
                draw_clutter=True
            else:
                draw_target=False
                draw_match=False
                draw_clutter=False

            if target_pos_x_deg==0:
                draw_target |= True
            if match_pos_x_deg==0:
                draw_match |= True

            if params.allow_stim_view:
                if (drawall==False) and not (gpos is None):
                    # If there is an eye position on the second fixation (down)
                    if (gpos[1]<-params.gc_fmatch_hide) and (abs(gpos[0])<thresh_pixels):
                        if match_pos_x_deg==0: # hide match, show target
                            draw_match &= False
                            draw_target |= True

            if self.state_current!=STATE_MENU:
                if draw_target:
                    [atarget.draw() for atarget in self.targets]
                if draw_match:
                    [atarget.draw() for atarget in self.matches]
                if draw_clutter:
                    [aclutter.draw() for aclutter in self.flankers]

            self.icons_top0.draw()
            self.icons_top1.draw()
            self.icons_top2.draw()
            self.icons_top3.draw()
            self.icons_top4.draw()

            if doflip:
                self.win.flip()

            if extra_key=='i':
                self.win.getMovieFrame()   # Defaults to front buffer, I.e. what's on screen now.
                self.win.saveMovieFrames('screenshot.png')

            if (drawall==False) and not (gpos is None) and not (self.state_current==STATE_MENU): # write position when masking
                if (self.timer_buzz.getTime()<0):
                    self.timer_buzz.reset(buzz_duration)
                    #sound_buzz.play()
                log_msg(self, "gaze", gpos[0], gpos[1])

            if done and params.enable_sounds:
                self.sound_done.play()

            return done,extra_key
        
    def finish(self):
        if self.win_mine:
            self.win.close()
        self.outfile.close()


def graft(target,expt, remove_flankers=True,pos_xoff=0):
    import replay
    previous_stims,prev_code=replay.rebuild(target, expt.win)
    for nstim,astim in enumerate( previous_stims.values() ): # Graft in current expt. outfile and time info

        astim.idcode=nstim+1000
        astim.outfile=expt.outfile
        astim.time_start=expt.time_start
        if astim.deleted==False:
            # Skip if deleted so it won't write out initial state
            astim.nudge('pos',[params.target_pos_x_deg*params.ppd+float(pos_xoff)*params.ppd,0]) 
            astim.nudge('ori',0) # to write out initial state

    expt.targets += previous_stims.values()
    if remove_flankers:
        expt.flankers = []
    #expt.num_matches=len(expt.matches)

if __name__ == "__main__":
    import sys 
    import argparse
    global delta_threshold
    parser = argparse.ArgumentParser()
    #parser.add_argument('filename', help='Filename')
    parser.add_argument('--append', help='Existing filename to append/continue..', default="", nargs='?') 
    parser.add_argument('--target', help='Use file as target', default="", nargs='?') 
    parser.add_argument('--debug', help='Show log messages', action='store_const', dest='debug', const=True, default=False  )
    parser.add_argument('--bare', help='No flankers, targets, clutter etc..', action='store_const', dest='bare', const=True, default=False  )
    parser.add_argument('--grid', help='Add black grid', action='store_const', dest='grid', const=True, default=False  )
    #parser.add_argument('--debug', help='Show log messages', action='store_const', dest='debug', const=True, default=False  )
    #parser.add_argument('--threshold', help='Smallest delta to consider a false jump', default=40, nargs='?'  )
    #parser.add_argument('--ignore', help='By default, ignore matches. Disable with this', action='store_const', dest='ignore', const=False, default=True   )
    parser.add_argument('--colorize', help='Whether to color all line stimuli', action='store_const', dest='colorize', const=True, default=False  )
    parser.add_argument('--word', help='Draw words', default="", nargs='?')

    args = parser.parse_args() 

    expt=adjust_experiment()

    if args.debug>0:
        logging.basicConfig(level=logging.INFO)

    if len(args.append)>0:
        expt.setup(default_stims=True, bare=args.bare) # don't create any stims
        expt.colorize=args.colorize
        # Start with existing edge information from file
        import replay
        previous_stims,code=replay.rebuild(args.append, expt.win, colorize=args.colorize)
		
        letters=[]
        matches=[]
        for nstim,astim in enumerate( previous_stims.values() ): # Graft in current expt. outfile and time info
            logging.info("appending n=%d id=%d"%(nstim, int(astim.idcode)) )
			
            if int(astim.idcode) > 1000:
				# The stimulus is from a custom-drawn match (i.e., a letter)
				astim._stim.pos[1] += 100 # move up
				letters += [astim]
            else:
				astim.idcode=nstim
				matches += [astim]
				
            astim.outfile=expt.outfile
            astim.time_start=expt.time_start
            # Skip if deleted so it won't write out initial state
            if astim.deleted==False:
                astim.nudge('ori',0) # to write out initial state
        expt.matches=matches #previous_stims.values()

        size=1.0*params.ppd
        lw=size/8.0

        args.grid=True
        if args.grid:
            #grid=stims.TwoLinesConnected(win=expt.win, name='', pos=(0,0), size=size, lineWidth=lw, edges=len(code)/2)#, color="black")
            #grid.update(params.stimGrid)
            #print params.stimGrid
            #expt.flankers=[grid]
            #expt.targets += [grid]
            code=params.stimGrid
            rand_pos=(0,0)
            randstim=stims.TwoLinesConnected(win=expt.win, name='', pos=rand_pos, size=size, lineWidth=lw, edges=len(code)/2, color="purple")#1.0*ppd/5.0, edges=edges )
        else:
            rand_pos=(0,100)
            randstim=stims.TwoLinesConnected(win=expt.win, name='', pos=rand_pos, size=size, lineWidth=lw, edges=len(code)/2, colorize=args.colorize )#1.0*ppd/5.0, edges=edges )

        #else:
            #expt.flankers=[]


        randstim.update(code)
        log_msg(expt, "lc:%s"%(code), 100, len(code)/2 )
        expt.targets=letters+[randstim]
        expt.flankers=[]

        expt.num_matches=len(expt.matches)
        expt.select_which=0 #len(expt.matches)
    elif len(args.target)>0:
        expt.setup(default_stims=True, rand_lines=[1,3], bare=args.bare) # add 1-2 lines
        expt.colorize=args.colorize
        # Start with existing edge information from file
        graft(args.target,expt)
    elif len(args.word)>0:
        expt.setup(default_stims=True )#, bare=True ) # add 1-2 lines
        expt.colorize=False

        for nlet,alet in enumerate(args.word):
            print nlet,alet,

            cap=alet.capitalize()
            if  (alet==cap):
                cap_which="upper"
            else:
                cap_which="lower"

            # Use added for interior letters demonstration
            if nlet>0 and nlet<(len(args.word)-1):
                fil="letters/ln_%s%s_double.csv"%(cap_which,cap)
            else:
                fil="letters/ln_%s%s.csv"%(cap_which,cap)

            graft(fil,expt, remove_flankers=True, pos_xoff=-8+1.5*nlet+7)
        ## Start with existing edge information from file
        #graft(args.target,expt)
    else:
        # no cmd-line args
        if len(params.target_letter)>0:
            expt.setup()
            expt.colorize=args.colorize
            graft(params.target_letter,expt, remove_flankers=False)
        else:
            expt.setup()
            expt.colorize=args.colorize
		
        try:
            if len(params.letter2)>0:
                graft(params.letter2,expt, remove_flankers=False, pos_xoff=params.pos2)
            if len(params.letter3)>0:
                graft(params.letter3,expt, remove_flankers=False, pos_xoff=params.pos3)
        except AttributeError: # no params.letter2
            pass

    while expt.do_frame(drawall=True)[0]==False: # while (done)==False
        pass
    expt.finish()
    
