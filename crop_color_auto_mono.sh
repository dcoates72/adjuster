# Run this with a number.
#
# It will call auto_stim.py to generate the CSV from the favorite
#   'simulating' an experiment using this stimulus.
# Then it calls replay.py to generate the png. 'screenshot' image from the CSV. 
# Then it crops the image.

#python auto_stim.py $1 > stim$1.csv
#python replay.py  --speed -1 --movie --nowait stim$1.csv

# Put result file in (maybe new) subdir called crop_<filename>
mkdir crop_$1

for f in `cat $1` ; do
    echo $f
    python replay.py  --speed -1 --movie --nowait --recolor $f
    #convert $f.png -crop 1024x100+0+340 crop_$1/${f##*/}.png
    convert $f.png -crop 824x100+100+340 crop_$1/${f##*/}.png
done

cd crop_$1
bash ../auto_html.sh
cd ..
