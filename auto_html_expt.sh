# Generate an HTML 'gallery', which is really just a big list of the images, with clickable thumbnails.

ls -1tr $@ |  awk 'BEGIN {print "<!DOCTYPE html><html><body>"}; {print $0,"<br><a href=\"",$0,"\"><img src=\"",$0,"\" width=1024 height=120></a>"}; END {print "</body></html>"};' > gallery.html

#ls -1tr $@ |  awk 'BEGIN {print "<!DOCTYPE html><html><body> <p style=\"font-size:10px\">"}; {print $0,"<a href=\"",$0,"\"><img src=\"",$0,"\" width=512 height=60></a>"}; END {print "</p></body></html>"};' > gallery.html


