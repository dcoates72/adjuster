def sequence_from_file(fname,which,locos):
    import csv
    with open(fname, 'rb') as f:
        lines=[row for row in csv.reader(f)]

    # Assume first line is header
    header_line=lines[0]

    # We will copy settings from the nth line of the file
    trial_line=lines[which]

    # Loop for each field specified in the header,
    # the set variables according to what is on the target/trial line
    for nfield,field in enumerate(header_line):
        if field=="target_code":
            which_code=trial_line[nfield]
            if not (which_code==""):
                val=locos[which_code]
            else:
                val=""
        else: # most fields will be processed here
                try:
                    val=int(trial_line[nfield])
                except ValueError:
                    val=trial_line[nfield]

        locos[field]=val
