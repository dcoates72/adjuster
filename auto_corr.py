import numpy as np
import extract_correspondence
import extract_correspondence as ec
import matplotlib.pyplot as plt

#results/S10_1.5deg_spac_2.5deg_0_t0_m0_04112017-07.csv 12 01
#results/S10_1.5deg_spac_2.5deg_1_t10_m-10_04102017-00.csv 42 #not sure!! 29 #L looks like random

conds=['F','P','B'] #,'C'] # fovea, peripheral, bi-peripheral, c=copy

def getcond(filename):
    cond=filename.split('_')
    if (cond[5]=='t0') and (cond[6]=='m-10'):
        code='P'
    elif (cond[5]=='t10') and (cond[6]=='m0'):
        code='F'
    elif (cond[5]=='t10') and (cond[6]=='m-10'):
        code='B'
    elif (cond[5]=='t0') and (cond[6]=='m0'):
        code='C'
    else:
        code='X'

    crowded=(cond[4]=='1')
    
    return code,crowded

def autocond(filename="../adjust_match_pilot/allres.txt", cond_show='F', flanked_show=False, whichp=0):
    fil=open(filename,"rt")
    lins=fil.readlines()

    vals=[]
    numsegs=[]
    numtargs=[]
    count=0
    for nlin,alin in enumerate(lins):
        fields=alin.split(' ')
        cond,crowded=getcond(fields[0])
        if fields[0][0]=='#':
            continue
        if (cond!=cond_show) or (crowded!=flanked_show):
            continue
        #print nlin,alin,cond,
	#print fields[1],

        rater0="../adjust_match_pilot/subs2_color/Sx_colors_04212017-%s.csv"%fields[1]
        segs,target,arr,valids=ec.extract(rater0)
        try:
                avals,bvals,diffs=ec.plotcorr( whichp, arr, target, valids, doplot=False)
                vals=np.append( vals,diffs  )
                numsegs = np.append( numsegs, len(segs) )
                numtargs = np.append( numtargs, len(target) )
                count += 1
        except IndexError:
                #	print "fail %s"%alin
                pass

    return vals,numsegs, numtargs,count

if __name__=="__main__":
    import argparse
    #parser = argparse.ArgumentParser()
    #parser.add_argument('filename', help='Filename')
    #parser.add_argument('--debug', help='Show log messages', action='store_const', dest='debug', const=True, default=False  )
    #args = parser.parse_args() 

    import pandas as pd
    #fig=plt.figure( figsize=(15,6) )
    fig, ax4 = plt.subplots(4, 4, figsize=(15,21) )

    fig2, ax_counts = plt.subplots(1, 1 )

    color_cond=['r','g','b']
    xlab=['xpos (deg)', 'ypos (deg)', 'angle (angular deg)', 'length (deg)']
    ylab=['Foveal match', 'Periph. match', 'Periph/Periph']
    lims=( [-2,2], [-1,1], [-50,50], [-2,2] )
    ls=[':','--','-']
    ls_crowded=['-','--']
    segnums=np.zeros( (3,10,10) )
    segnums_crowded=np.zeros( (3,10,10) )
    nsegs=np.zeros( (3,6) )
    nsegs_crowded=np.zeros( (3,6) )

    if True:
            pnum=0
            ncond=0
            cond='P'
            whichp=pnum
            diffs,nseg,ntarg,count=autocond(cond_show=cond,whichp=whichp)

            plt.figure()
            plt.hist( diffs, normed=True, alpha='0.5', color='g' )
            plt.hist( diffs, normed=True, alpha='0.5', color='g' )
            plt.xlim( -1, 1)

    exit()

    for ncond,cond in enumerate( ['F','P','B']):
        for pnum in np.arange(4):
            whichp=pnum
            ax=ax4[ncond,pnum]
            plt.sca(ax)

            diffs,nseg,ntarg,count=autocond(cond_show=cond,whichp=whichp)

            popt,pcov=ec.fitgauss(
                *extract_correspondence.fitk( diffs ) )

            label='%s_'%cond
            label_long='%s %0.2f %0.2f %0.2f'%(label,popt[0],popt[1],popt[2])
            df=pd.DataFrame( diffs, columns=[label_long])
            df.plot(kind='density', ax=ax, ls=ls_crowded[0], color=color_cond[ncond])

            diffs2,nseg2,ntarg2,count2=autocond(cond_show=cond,flanked_show=True,whichp=whichp)
            popt2,pcov2=extract_correspondence.fitgauss(
                *extract_correspondence.fitk( diffs2 ) )

            label2='%sX'%(cond)
            label_long2='%s %0.2f %0.2f %0.2f'%(label2,popt2[0],popt2[1],popt2[2])
            df2=pd.DataFrame( diffs2, columns=[label_long2])
            df2.plot(kind='density', ax=ax, ls=ls_crowded[1], color=color_cond[ncond])

            np.save('%s-%d'%(label,pnum), np.array(df))
            np.save('%s-%d'%(label2,pnum), np.array(df2))
            if (pnum==0):
                for ns,nt in zip(nseg,ntarg):
                    segnums[ncond,int(ns),int(nt)] += 1
                for ns,nt in zip(nseg2,ntarg2):
                    segnums_crowded[ncond,int(ns),int(nt)] += 1
                print count,count2
                print ncond, np.sum(segnums[ncond])
                print ncond, np.sum(segnums_crowded[ncond])

            plt.xlim( lims[pnum] )
            plt.grid()

            if ncond in [0,2]:
                plt.xlabel( xlab[pnum], size=18 )

            if pnum==0:
                plt.ylabel( ylab[ncond], size=18 )
            else:
                plt.ylabel('')

            if pnum==0:
                plt.title("N=%d X=%d"%(len(diffs),len(diffs2) ), size=11 )

            ax=ax4[3,pnum]
            plt.sca(ax)



            df.plot(kind='density', ax=ax, ls=ls_crowded[0], color=color_cond[ncond])
            df2.plot(kind='density', ax=ax, ls=ls_crowded[1], color=color_cond[ncond])
            plt.xlim( lims[pnum] )
            plt.grid( True )
            ax.legend_.remove()
            

            if pnum==0:
                #plt.sca(ax_counts)
                for n in [4,5]:
                    nsegs[ncond,0] += segnums[ncond,n-2,n]
                    nsegs[ncond,1] += segnums[ncond,n-1,n]
                    nsegs[ncond,2] += segnums[ncond,n,n]
                    nsegs[ncond,3] += segnums[ncond,n+1,n]
                    nsegs[ncond,4] += segnums[ncond,n+2,n]
                    nsegs[ncond,5] += segnums[ncond,n+3,n]
                    #plt.plot( np.arange(10)-n, segnums_crowded[ncond,:,n], 'o-', label='%s'%cond );
                    #plt.grid(True)
                for n in [4,5]:
                    nsegs_crowded[ncond,0] += segnums_crowded[ncond,n-2,n]
                    nsegs_crowded[ncond,1] += segnums_crowded[ncond,n-1,n]
                    nsegs_crowded[ncond,2] += segnums_crowded[ncond,n,n]
                    nsegs_crowded[ncond,3] += segnums_crowded[ncond,n+1,n]
                    nsegs_crowded[ncond,4] += segnums_crowded[ncond,n+2,n]
                    nsegs_crowded[ncond,5] += segnums_crowded[ncond,n+3,n]
                    #plt.plot( np.arange(10)-n, segnums_crowded[ncond,:,n], 'o-', label='%s'%cond );
                    #plt.grid(True)

        #print
    plt.sca(ax_counts)
    xr=np.arange(6)-2.0
    xr_smooth=np.linspace(xr[0], xr[-1],200)
    for n in [0,1,2]:
        y1=nsegs[n]/np.sum(nsegs[n])
        popt,pcov=extract_correspondence.fitgauss(xr,y1, interp=True)
        label_long='%s_ %0.2f %0.2f %0.2f'%(conds[n],popt[0],popt[1],popt[2])
        plt.plot( xr, y1 , 'o', color=color_cond[n], ls='', label=label_long );
        #plt.plot( xr, y1 , 'o', color=color_cond[n], ls=ls_crowded[0], label=label_long );
        fitted=extract_correspondence.gaus(xr_smooth, *popt )
        plt.plot( xr_smooth,fitted, '', ls=ls_crowded[0], color=color_cond[n] )

        y2=nsegs_crowded[n]/np.sum(nsegs_crowded[n])
        popt,pcov=extract_correspondence.fitgauss(xr,y2, interp=True)
        label_long='%sX %0.2f %0.2f %0.2f'%(conds[n],popt[0],popt[1],popt[2])

        #plt.plot( xr, y2, 'o', color=color_cond[n], ls=ls_crowded[1], label=label_long );
        plt.plot( xr, y2, 'o', markeredgecolor=color_cond[n], markerfacecolor='none',
            ls='', label=label_long );

        fitted=extract_correspondence.gaus(xr_smooth, *popt )
        plt.plot( xr_smooth,fitted, '', ls=ls_crowded[1], color=color_cond[n] )

        plt.grid(True)

    plt.xlabel("# segs (match) - # segs (target)", size=16)
    plt.ylabel("Prop. trials", size=16)
    plt.legend( loc='best' )
    plt.savefig("counts.pdf")

    plt.sca(ax)
    plt.subplots_adjust( hspace=0.5 )
    plt.show()
    plt.savefig("histograms.pdf")
   
